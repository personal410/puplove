//
//  Message.h
//  bawow
//
//  Created by victor salazar on 30/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class User;
@interface Message:NSManagedObject{
    CGRect frame;
}
@property (nonatomic, retain) NSString * messageId;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) User *sender;
@property (nonatomic, retain) User *reciever;
-(void)setFrame:(CGRect)newFrame;
-(CGRect)getFrame;
@end