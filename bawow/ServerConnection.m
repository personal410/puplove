//
//  ServerConnection.m
//  bawow
//
//  Created by victor salazar on 14/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ServerConnection.h"
@implementation ServerConnection
+(id)sharedServerConnection{
    static ServerConnection *sharedServerConnection = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedServerConnection = [[ServerConnection alloc] init];
    });
    return sharedServerConnection;
}
-(instancetype)init{
    if(self = [super init]){
        arrRequests = [NSMutableArray arrayWithCapacity:0];
        arrCompletitionHandler = [NSMutableArray arrayWithCapacity:0];
    }
    return self;
}
+(void)addRequest:(NSURLRequest *)request WithCompletitionHandler:(completitionHandler)handler{
    [[ServerConnection sharedServerConnection] addRequest:request WithCompletitionHandler:handler];
}
-(void)addRequest:(NSURLRequest *)request WithCompletitionHandler:(completitionHandler)handler{
    [arrRequests addObject:request];
    [arrCompletitionHandler addObject:handler];
    [self sendRequest];
}
-(void)sendRequest{
    if(arrRequests.count > 0 && !isConected){
        NSURLRequest *req = [arrRequests firstObject];
        completitionHandler handler = [arrCompletitionHandler firstObject];
        [arrRequests removeObjectAtIndex:0];
        [arrCompletitionHandler removeObjectAtIndex:0];
        isConected = YES;
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
            handler(data, connectionError);
            isConected = NO;
            [self sendRequest];
        }];
    }
}
@end