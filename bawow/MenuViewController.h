//
//  MenuViewController.h
//  bawow
//
//  Created by victor salazar on 5/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UsuarioViewController.h"
@interface MenuViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,weak) IBOutlet UIImageView *profileImgView;
-(void)actualizarFotoPerfil;
@end
