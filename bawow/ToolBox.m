//
//  ToolBox.m
//  bawow
//
//  Created by victor salazar on 22/08/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ToolBox.h"
@implementation ToolBox
+(NSDateFormatter *)preconfiguredDateFormatter{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"es_ES"];
    return dateFormatter;
}
+(NSString *)jsonObjectToString:(id)json{
    NSError *jsonError = nil;
    NSData *dataTemp = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:&jsonError];
    if(jsonError){
        return nil;
    }else{
        return [[NSString alloc] initWithData:dataTemp encoding:NSUTF8StringEncoding];
    }
}
+(NSString *)dictionaryToPostParams:(NSDictionary *)dictionary{
    NSMutableString *strFinal = [NSMutableString new];
    for(NSString *key in dictionary.allKeys){
        [strFinal appendFormat:@"%@=%@", key, [dictionary objectForKey:key]];
        if(![key isEqual:dictionary.allKeys.lastObject]){
            [strFinal appendString:@"&"];
        }
    }
    return strFinal;
}
+(NSURLRequest *)createRequestWithUrl:(NSString *)strUrl withParams:(id)params{
    return [self createRequestWithUrl:strUrl withParams:params withMethod:@"POST"];
}
+(NSURLRequest *)createRequestWithUrl:(NSString *)strUrl withParams:(id)params withMethod:(NSString *)method{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:method];
    if(params){
        if([params isKindOfClass:[NSString class]]) {
            NSString *strParams = params;
            [request setHTTPBody:[strParams dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    return request;
}
+(BOOL)isErrorConnectionInternet:(NSInteger)errorCode{
    if((errorCode == NSURLErrorCannotFindHost) || (errorCode == NSURLErrorCannotConnectToHost) || (errorCode == NSURLErrorNetworkConnectionLost) || (errorCode == NSURLErrorDNSLookupFailed) || (errorCode == NSURLErrorHTTPTooManyRedirects) || (errorCode == NSURLErrorNotConnectedToInternet) || (errorCode == NSURLErrorSecureConnectionFailed) || (errorCode == NSURLErrorCannotLoadFromNetwork)){
        return true;
    }else{
        return false;
    }
}
+(void)showConnectionErrorInViewCont:(id)viewCont{
    [self showErrorWithMessage:@"Unable to connect to the internet." inViewCont:viewCont];
}
+(void)showGenericErrorInViewCont:(id)viewCont{
    [self showErrorWithMessage:@"There was an error, try again." inViewCont:viewCont];
}
+(void)showErrorWithMessage:(NSString *)thisMessage inViewCont:(id)viewCont{
    [self showAlertWith:@"Error" and:thisMessage inViewCont:viewCont];
}
+(void)showAlertWith:(NSString *)thisTitle and:(NSString *)thisMessage inViewCont:(id)viewCont{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:thisTitle message:thisMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:defaultAction];
    [viewCont presentViewController:alertCont animated:true completion:nil];
}
@end
