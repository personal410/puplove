//
//  CropPhotoViewController.h
//  bawow
//
//  Created by victor salazar on 1/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface CropPhotoViewController:UIViewController<UIScrollViewDelegate>{
    UIImageView *imgView;
}
@property(nonatomic,weak) IBOutlet UIScrollView *imgViewScrollView;
@property(nonatomic,weak) IBOutlet UIView *topView;
@property(nonatomic,weak) IBOutlet UIView *bottomView;
@property(nonatomic,strong) UIImage *image;
@end