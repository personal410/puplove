//
//  PhotoCollectionViewController.h
//  bawow
//
//  Created by victor salazar on 29/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "PhotoFBCollectionViewCell.h"
#import "CropPhotoViewController.h"
@interface PhotoFBCollectionViewController:UICollectionViewController<UICollectionViewDelegateFlowLayout>{
    UIImage *imgTemp;
    BOOL selectedImage;
}
@property(nonatomic,strong) NSMutableArray *arrPhotos;
@property(nonatomic,strong) NSString *albumId;
@end