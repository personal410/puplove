//
//  PhotoCollectionViewCell.h
//  bawow
//
//  Created by victor salazar on 29/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface PhotoCollectionViewCell:UICollectionViewCell
@property(nonatomic,weak) IBOutlet UIImageView *photoImgView;
@property(nonatomic,weak) IBOutlet UIView *whiteView;
@property(nonatomic,weak) IBOutlet UIView *addOrRemoveView;
@end