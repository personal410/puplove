//
//  CropPhotoViewController.m
//  bawow
//
//  Created by victor salazar on 1/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "CropPhotoViewController.h"
@implementation CropPhotoViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.imgViewScrollView.layer.borderWidth = 1;
    self.imgViewScrollView.layer.borderColor = [[UIColor whiteColor] CGColor];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    imgView = [[UIImageView alloc] initWithImage:self.image];
    self.imgViewScrollView.contentSize = imgView.frame.size;
    [self.imgViewScrollView addSubview:imgView];
    float scaleWidth = self.imgViewScrollView.frame.size.width / imgView.frame.size.width;
    float scaleHeight = self.imgViewScrollView.frame.size.height / imgView.frame.size.height;
    float minScale = MAX(scaleWidth, scaleHeight);
    self.imgViewScrollView.minimumZoomScale = minScale;
    self.imgViewScrollView.maximumZoomScale = 1;
    self.imgViewScrollView.zoomScale = minScale;
    if(imgView.frame.size.width > self.imgViewScrollView.frame.size.width){
        float x = (imgView.frame.size.width - self.imgViewScrollView.frame.size.width)/2.0;
        self.imgViewScrollView.contentOffset = CGPointMake(x, 0);
    }
    if(imgView.frame.size.height > self.imgViewScrollView.frame.size.height){
        float y = (imgView.frame.size.height - self.imgViewScrollView.frame.size.height)/2.0;
        self.imgViewScrollView.contentOffset = CGPointMake(0, y);
    }
    [self centerScrollViewContents];
}
-(void)centerScrollViewContents{
    CGSize boundsSize = self.imgViewScrollView.bounds.size;
    CGRect contentsFrame = imgView.frame;
    if(contentsFrame.size.width < boundsSize.width){
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width)/2.0f;
    }else{
        contentsFrame.origin.x = 0.0f;
    }
    if(contentsFrame.size.height < boundsSize.height){
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height)/2.0f;
    }else{
        contentsFrame.origin.y = 0.0f;
    }
    imgView.frame = contentsFrame;
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return imgView;
}
-(void)scrollViewDidZoom:(UIScrollView *)scrollView{
    [self centerScrollViewContents];
}
-(IBAction)cancelar:(id)sender{
    if(self.navigationController){
        [self.navigationController setNavigationBarHidden:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(IBAction)elegir:(id)sender{
    [[Diccionario sharedDiccionario] setUploadPhotos:YES];
    UIGraphicsBeginImageContext(imgView.frame.size);
    [self.image drawInRect:CGRectMake(0, 0, imgView.frame.size.width, imgView.frame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGRect clippedRect = CGRectMake(self.imgViewScrollView.contentOffset.x, self.imgViewScrollView.contentOffset.y, 320, 320);
    CGImageRef imageRef = CGImageCreateWithImageInRect(newImage.CGImage, clippedRect);
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    NSData *dataFinalImage = UIImagePNGRepresentation(finalImage);
    User *user = [UserController getUser];
    NSSet *photos = user.profile.photos;
    int cant = (int)photos.count;
    int ind = cant + 1;
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    Photo *photoTemp = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
    photoTemp.indPhoto = @(ind);
    photoTemp.profile = user.profile;
    photoTemp.photo = dataFinalImage;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end