//
//  UserTableViewCell.h
//  bawow
//
//  Created by victor salazar on 13/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface FriendTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *profileImgView;
@property(nonatomic,weak) IBOutlet UILabel *usernameLbl;
@property(nonatomic,weak) IBOutlet UILabel *matchedOnLbl;
@end