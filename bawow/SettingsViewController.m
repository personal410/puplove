//
//  SettingsViewController.m
//  bawow
//
//  Created by victor salazar on 14/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "SettingsViewController.h"
#import "SearchDistTableViewCell.h"
#import "TermsOrPrivacyTableViewCell.h"
@implementation SettingsViewController
-(void)viewDidLoad{
    [super viewDidLoad];
}
#pragma mark - UITableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 80;
    }else{
        return 70;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        SearchDistTableViewCell *searchDistanceCell = [tableView dequeueReusableCellWithIdentifier:@"searchDistanceCell" forIndexPath:indexPath];
        float searchDist = [[[UserController getUser] searchDistance] floatValue];
        searchDistanceCell.searchDistanceSlider.value = searchDist;
        searchDistanceCell.valueSearchDistLbl.text = [NSString stringWithFormat:@"%.0fKm", searchDist];
        return searchDistanceCell;
    }else{
        TermsOrPrivacyTableViewCell *termsAndPrivPolCell = [tableView dequeueReusableCellWithIdentifier:@"termsAndPrivPolCell" forIndexPath:indexPath];
        if(indexPath.row == 1){
            [termsAndPrivPolCell.termsOrPrivacyBtn setTitle:@"Terms of Agreement" forState:UIControlStateNormal];
        }else{
            [termsAndPrivPolCell.termsOrPrivacyBtn setTitle:@"Privacy Policy" forState:UIControlStateNormal];
        }
        termsAndPrivPolCell.termsOrPrivacyBtn.tag = indexPath.row;
        termsAndPrivPolCell.termsOrPrivacyBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        return termsAndPrivPolCell;
    }
}
#pragma mark - IBAction
-(IBAction)logout:(id)sender{
    [[FBSession activeSession] closeAndClearTokenInformation];
    [UserController deleteUser];
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"iniSesionViewCont"];
    UIWindow *window = (UIWindow *)[[UIApplication sharedApplication].windows firstObject];
    [UIView transitionFromView:window.rootViewController.view toView:viewCont.view duration:0.65 options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished) {
        window.rootViewController = viewCont;
    }];
}
-(IBAction)changeSearchDistance:(id)sender{
    SearchDistTableViewCell *searchDistCell = (SearchDistTableViewCell *)[self.settingsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    float valueSearchDist = searchDistCell.searchDistanceSlider.value;
    float valueTemp = roundf(valueSearchDist/10.0);
    valueTemp = valueTemp*10;
    [UserController updateUser:[UserController getUser] withDictionary:@{@"searchDistance": [NSNumber numberWithFloat:valueTemp]}];
    searchDistCell.valueSearchDistLbl.text = [NSString stringWithFormat:@"%.0fKm", valueTemp];
    [[Diccionario sharedDiccionario] setDidChangeSearchDistance:true];
}
-(IBAction)termsAndPrivPol:(UIButton *)sender{
    if(sender.tag == 1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[URLs urlTermsAgreement]]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[URLs urlPrivacyPolicy]]];
    }
}
@end