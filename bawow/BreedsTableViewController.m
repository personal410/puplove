//
//  BreedsTableViewController.m
//  bawow
//
//  Created by victor salazar on 6/05/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "BreedsTableViewController.h"
@implementation BreedsTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrBreeds = [[Diccionario sharedDiccionario] arrRazas];
    indSeleccionado = (self.currentUser.profile.breed)?(int)[arrBreeds indexOfObject:self.currentUser.profile.breed]:-1;
    resultSearchBreedsTableViewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultBreedTableViewCont"];
    searchCont = [[UISearchController alloc] initWithSearchResultsController:resultSearchBreedsTableViewCont];
    searchCont.hidesNavigationBarDuringPresentation = true;
    searchCont.searchResultsUpdater = self;
    searchCont.delegate = self;
    searchCont.searchBar.translucent = true;
    searchCont.searchBar.searchBarStyle = UISearchBarStyleDefault;
    searchCont.searchBar.barStyle = UIBarStyleDefault;
    searchCont.searchBar.frame = CGRectMake(0, 0, searchCont.searchBar.frame.size.width, 44.0);
    self.definesPresentationContext = true;
    self.tableView.tableHeaderView = searchCont.searchBar;
    [self.tableView setContentOffset:CGPointMake(0, 44) animated:false];
    if(indSeleccionado == -1){
        self.navigationItem.rightBarButtonItem.enabled = false;
    }
}
#pragma mark - Table view data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrBreeds.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = arrBreeds[indexPath.row];
    if(indexPath.row == indSeleccionado){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != indSeleccionado){
        indSeleccionado = (int)indexPath.row;
        self.currentBreed = arrBreeds[indSeleccionado];
        self.navigationItem.rightBarButtonItem.enabled = true;
        [self.tableView reloadData];
    }
}
-(IBAction)actionOk:(id)sender{
    self.currentUser.profile.breed = arrBreeds[indSeleccionado];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self contains[c] %@",searchCont.searchBar.text];
    NSArray *arrResult = [arrBreeds filteredArrayUsingPredicate:resultPredicate];
    resultSearchBreedsTableViewCont.arrResult = arrResult;
    resultSearchBreedsTableViewCont.indSeleccionado = ([arrResult indexOfObject:self.currentBreed] == NSNotFound)?-1:(int)[arrResult indexOfObject:self.currentBreed];
    [resultSearchBreedsTableViewCont.tableView reloadData];
}
-(void)willPresentSearchController:(UISearchController *)searchController{
    self.navigationController.navigationBar.translucent = true;
}
-(void)willDismissSearchController:(UISearchController *)searchController{
    self.navigationController.navigationBar.translucent = false;
    if(resultSearchBreedsTableViewCont.indSeleccionado != -1){
        NSString *breedSelected = [resultSearchBreedsTableViewCont.arrResult objectAtIndex:resultSearchBreedsTableViewCont.indSeleccionado];
        indSeleccionado = (int)[arrBreeds indexOfObject:breedSelected];
        self.navigationItem.rightBarButtonItem.enabled = true;
        [self.tableView reloadData];
    }
}
@end