//
//  MatchViewController.h
//  bawow
//
//  Created by victor salazar on 24/07/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface MatchViewController:UIViewController
@property(nonatomic,strong) User *userFriend;
@property(nonatomic,weak) IBOutlet UIImageView *userImgView;
@property(nonatomic,weak) IBOutlet UIImageView *friendImgView;
@end