//
//  Diccionario.h
//  bawow
//
//  Created by victor salazar on 7/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface Diccionario:NSObject
@property(nonatomic) int indIdioma;
@property(nonatomic) BOOL uploadPhotos;
@property(nonatomic) BOOL didChangeSearchDistance;
+(id)sharedDiccionario;
-(NSString *)stringGuardar;
-(NSString *)stringFotosDeMi;
-(NSString *)stringFotos;
-(NSString *)stringCancelar;
-(NSString *)stringEliminarFoto;
-(NSString *)stringLibreria;
-(NSString *)stringAtras;
-(NSString *)stringAlerta;
-(NSString *)stringSinFotos;
-(NSString *)strEnviar;
-(NSString *)strAnioConNumero:(int)numero;
-(NSString *)strMesConNumero:(int)numero;
-(NSArray *)arrOpcionesMenu;
-(NSArray *)arrViewConts;
-(NSArray *)arrOpcionesSegCtrlConfPer;
-(NSArray *)arrValoresOpcSegCtrlGender;
-(NSArray *)arrRazas;
@end