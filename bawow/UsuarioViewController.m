//
//  UsuarioViewController.m
//  bawow
//
//  Created by victor salazar on 1/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "UsuarioViewController.h"
#import "MessageController.h"
@implementation UsuarioViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.panEnabled = true;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if(segue.identifier){
        if([segue.identifier isEqualToString:@"playSegue"]){
            playViewCont = segue.destinationViewController;
        }
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self syncUser];
}
-(void)syncUser{
    User *user = [UserController getUser];
    NSArray *friends = [UserController getFriends];
    NSMutableArray *arrSyncUsers = [NSMutableArray array];
    for(User *friend in friends){
        NSArray *arrMessagesTemp = [MessageController getConversationWithUser:friend withOffset:0 withCount:1];
        NSString *strMessageId = @"";
        if(arrMessagesTemp.count > 0){
            strMessageId = [arrMessagesTemp.firstObject messageId];
        }
        [arrSyncUsers addObject:@{@"user_id": [NSString stringWithFormat:@"'%@'", friend.userId], @"message_id": strMessageId}];
    }
    NSDictionary *dicParams = @{@"sync": [ToolBox jsonObjectToString:arrSyncUsers]};
    NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlSyncUserWithUserId:user.userId] withParams:[ToolBox dictionaryToPostParams:dicParams]];
    [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *data, NSError *error){
        if(error == nil){
            NSError *jsonError;
            NSDictionary *dicResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            if(jsonError == nil){
                NSArray *arrUsersTemp = [dicResult objectForKey:@"matches"];
                for(NSDictionary *dicUserTemp in arrUsersTemp){
                    NSDictionary *dicUser = dicUserTemp[@"user"];
                    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                    User *userTemp = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                    userTemp.userId = dicUser[@"id"];
                    NSString *strProfAvatar = dicUser[@"profile_avatar"];
                    NSString *strFinalAvatar = [NSString stringWithFormat:@"%@%@", [URLs urlIp], strProfAvatar];
                    NSURL *urlAvatar = [NSURL URLWithString:strFinalAvatar];
                    userTemp.avatar = [NSData dataWithContentsOfURL:urlAvatar];
                    userTemp.email = dicUser[@"email"];
                    userTemp.fbUserId = dicUser[@"fb_user_id"];
                    userTemp.firstName = dicUser[@"first_name"];
                    userTemp.lastName = dicUser[@"last_name"];
                    userTemp.me = [NSNumber numberWithBool:false];
                    userTemp.lastMessage = @"";
                    NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
                    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
                    userTemp.matchOn = [dateFormatter dateFromString:[dicUser objectForKey:@"match_date"]];
                    NSDictionary *dicConversation = dicUser[@"conversation"];
                    userTemp.channel = dicConversation[@"channel"];
                    userTemp.token = dicConversation[@"token"];
                    NSError *error = nil;
                    if(![context save:&error]){
                        //[ToolBox showGenericErrorInViewCont:self];
                    }
                }
                NSArray *arrNotifications = [dicResult objectForKey:@"notifications"];
                for(NSDictionary *dicMessage in arrNotifications){
                    NSString *strUserId = [dicMessage objectForKey:@"user"];
                    NSString *strRecipientId = [dicMessage objectForKey:@"recipient"];
                    NSString *strFinalUserId = [user.userId isEqualToString:strUserId] ? strRecipientId : strUserId;
                    NSString *lastMessage = [dicMessage objectForKey:@"message"];
                    User *friend = [UserController getUserWithUserId:strFinalUserId];
                    [UserController updateUser:friend withDictionary:@{@"lastMessage": lastMessage}];
                }
            }else{
                //[ToolBox showGenericErrorInViewCont:self];
            }
        }else{
            NSLog(@"%s error: %@", __FUNCTION__, error);
            //[ToolBox showConnectionErrorInViewCont:self];
        }
        [self performSelector:@selector(syncUser) withObject:nil afterDelay:60];
    }];
}
-(void)showOrHideMenu{
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        if(!showingMenu){
            self.contentView.transform = CGAffineTransformMakeTranslation(self.view.frame.size.width - 90, 0);
            self.backgroundImgView2.transform = CGAffineTransformMakeTranslation(self.view.frame.size.width - 90, 0);
        }else{
            self.contentView.transform = CGAffineTransformIdentity;
            self.backgroundImgView2.transform = CGAffineTransformIdentity;
        }
    } completion:^(BOOL finished){
        showingMenu = !showingMenu;
        [self evaluateShowingMenu];
    }];
}
-(void)cambiarViewCont:(int)ind{
    [self showOrHideMenu];
    NSString *strViewContId = [[[Diccionario sharedDiccionario] arrViewConts] objectAtIndex:ind];
    [[self.contentView.subviews firstObject] removeFromSuperview];
    NSArray *arrSubViewConts = [self childViewControllers];
    for(id childViewCont in arrSubViewConts){
        if(![childViewCont isKindOfClass:[MenuViewController class]]){
            [childViewCont removeFromParentViewController];
            break;
        }
    }
    UIViewController *viewContTemp = nil;
    if([strViewContId isEqualToString:@"play"]){
        if(playViewCont){
            viewContTemp = playViewCont;
        }else{
            viewContTemp = [self.storyboard instantiateViewControllerWithIdentifier:[NSString stringWithFormat:@"%@ViewCont", strViewContId]];
            playViewCont = (UINavigationController *)viewContTemp;
        }
    }else{
        viewContTemp = [self.storyboard instantiateViewControllerWithIdentifier:[NSString stringWithFormat:@"%@ViewCont", strViewContId]];
    }
    viewContTemp.view.frame = self.contentView.frame;
    [self.contentView addSubview:viewContTemp.view];
    [self addChildViewController:viewContTemp];
}
-(IBAction)panGesture:(UIPanGestureRecognizer *)gesture{
    if(self.panEnabled){
        float minX = 0;
        float maxX = CGRectGetWidth(self.contentView.frame) - 90;
        float translateX = self.contentView.transform.tx;
        CGPoint puntoTemp = [gesture locationInView:self.view];
        if(gesture.state == UIGestureRecognizerStateBegan){
            puntoInicial = puntoTemp;
        }else if(gesture.state == UIGestureRecognizerStateChanged){
            float deltaX = translateX + puntoTemp.x - puntoInicial.x;
            if (deltaX < minX) {
                deltaX = minX;
            }else if (deltaX > maxX){
                deltaX = maxX;
            }
            self.contentView.transform = CGAffineTransformMakeTranslation(deltaX, 0);
            self.backgroundImgView2.transform = CGAffineTransformMakeTranslation(deltaX, 0);
            puntoInicial = puntoTemp;
        }else if(gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled){
            float semiDiff = (maxX - minX)/2.0 + minX;
            float posX = (translateX < semiDiff)?minX:maxX;
            [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.contentView.transform = CGAffineTransformMakeTranslation(posX, 0);
                self.backgroundImgView2.transform = CGAffineTransformMakeTranslation(posX, 0);
            } completion:^(BOOL finished){
                showingMenu = !(posX == minX);
                [self evaluateShowingMenu];
            }];
        }
    }
}
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint puntoTemp = [gestureRecognizer locationInView:self.view];
    if (!showingMenu && puntoTemp.x > 50) {
        return false;
    }
    if(showingMenu && puntoTemp.x < self.view.frame.size.width - 90){
        return false;
    }
    return true;
}
-(void)showConfProfile{
    [self performSegueWithIdentifier:@"mostrarConfPerfil" sender:self];
}
-(void)evaluateShowingMenu{
    for(id viewCont in self.childViewControllers){
        if(![viewCont isKindOfClass:[MenuViewController class]]){
            [[[viewCont topViewController] view] setUserInteractionEnabled:!showingMenu];
            break;
        }
    }
}
@end
