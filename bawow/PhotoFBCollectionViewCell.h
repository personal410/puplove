//
//  PhotoFBCollectionViewCell.h
//  bawow
//
//  Created by victor salazar on 29/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface PhotoFBCollectionViewCell:UICollectionViewCell
@property(nonatomic,weak) IBOutlet UIImageView *imgView;
@property(nonatomic,weak) IBOutlet UIView *blackView;
@property(nonatomic,weak) IBOutlet UIActivityIndicatorView *loadActIndView;
@end