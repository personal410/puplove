//
//  IniciarSesionViewController.h
//  bawow
//
//  Created by victor salazar on 1/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface IniciarSesionViewController:UIViewController<FBLoginViewDelegate>
@property(nonatomic,weak) IBOutlet UIImageView *dogImgView;
@property(nonatomic,weak) IBOutlet UIImageView *titleImgView;
@property(nonatomic,weak) IBOutlet FBLoginView *loginView;
@end
