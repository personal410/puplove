//
//  SentDateTableViewCell.h
//  bawow
//
//  Created by victor salazar on 4/04/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MessageTableViewCell.h"
@interface SentDateTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *sentDateLbl;
@property(nonatomic,strong) NSDate *date;
@property(nonatomic) Side side;
@end