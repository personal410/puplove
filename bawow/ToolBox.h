//
//  ToolBox.h
//  bawow
//
//  Created by victor salazar on 22/08/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface ToolBox:NSObject
+(NSDateFormatter *)preconfiguredDateFormatter;
+(NSString *)jsonObjectToString:(id)json;
+(NSData *)dictionaryToPostParams:(NSDictionary *)dictionary;
+(NSURLRequest *)createRequestWithUrl:(NSString *)strUrl withParams:(id)params;
+(NSURLRequest *)createRequestWithUrl:(NSString *)strUrl withParams:(id)params withMethod:(NSString *)method;
+(BOOL)isErrorConnectionInternet:(NSInteger)errorCode;
+(void)showConnectionErrorInViewCont:(id)viewCont;
+(void)showGenericErrorInViewCont:(id)viewCont;
+(void)showAlertWith:(NSString *)thisTitle and:(NSString *)thisMessage inViewCont:(id)viewCont;
@end
