//
//  ResultSearchBreedsTableViewController.m
//  bawow
//
//  Created by victor salazar on 29/07/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ResultSearchBreedsTableViewController.h"
@implementation ResultSearchBreedsTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
}
#pragma mark - Table view data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrResult.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = self.arrResult[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryNone;
    if(indexPath.row == self.indSeleccionado){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != self.indSeleccionado){
        self.indSeleccionado = (int)indexPath.row;
        [self.tableView reloadData];
    }
}
@end