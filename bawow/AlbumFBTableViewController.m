//
//  AlbumTableViewController.m
//  bawow
//
//  Created by victor salazar on 28/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "AlbumFBTableViewController.h"
@implementation AlbumFBTableViewController
-(void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    arrAlbums = [NSMutableArray arrayWithCapacity:0];
    arrPhotosOfMe = [NSMutableArray arrayWithCapacity:0];
    [FBRequestConnection startWithGraphPath:@"/me/albums" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *con, NSDictionary *result, NSError *error){
        if(error == nil){
            NSArray *data = [result objectForKey:@"data"];
            [arrAlbums addObjectsFromArray:data];
            [self getPhotosWithPath:@"/me/photos"];
        }else{
            NSLog(@"%s error: %@", __FUNCTION__, error);
            [ToolBox showConnectionErrorInViewCont:self];
        }
    }];
    UIBarButtonItem *cancelBarBtnItem = [[UIBarButtonItem alloc] initWithTitle:[[Diccionario sharedDiccionario] stringCancelar] style:UIBarButtonItemStyleDone target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = cancelBarBtnItem;
}
-(void)getPhotosWithPath:(NSString *)path{
    [FBRequestConnection startWithGraphPath:path parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *con, NSDictionary *result2, NSError *error){
        if(error == nil){
            NSArray *arr = [result2 objectForKey:@"data"];
            [arrPhotosOfMe addObjectsFromArray:arr];
            NSDictionary *paging = [result2 objectForKey:@"paging"];
            NSString *strNextPath = [paging objectForKey:@"next"];
            if(strNextPath){
                NSArray *arrNextPath = [strNextPath componentsSeparatedByString:@"?"];
                NSString *path = [NSString stringWithFormat:@"/me/photos?%@", [arrNextPath lastObject]];
                [self getPhotosWithPath:path];
            }else{
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:0];
                [dic setObject:[[Diccionario sharedDiccionario] stringFotosDeMi] forKey:@"name"];
                [dic setObject:[NSNumber numberWithInteger:arrPhotosOfMe.count] forKey:@"count"];
                [dic setObject:arrPhotosOfMe forKey:@"photos"];
                [arrAlbums addObject:dic];
                [self.tableView reloadData];
            }
        }else{
            NSLog(@"%s error: %@", __FUNCTION__, error);
            [ToolBox showConnectionErrorInViewCont:self];
        }
    }];
}
-(IBAction)cancel:(id)sender{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrAlbums.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"albumCell" forIndexPath:indexPath];
    NSDictionary *dicAlbum = [arrAlbums objectAtIndex:indexPath.row];
    cell.textLabel.text = [dicAlbum objectForKey:@"name"];
    int cant = [[dicAlbum objectForKey:@"count"] intValue];
    NSString *strPhoto = (cant == 1)?@"photo":@"photos";
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i %@", cant, strPhoto];
    if([dicAlbum isEqual:[arrAlbums lastObject]]){
        NSArray *photos = [dicAlbum objectForKey:@"photos"];
        NSDictionary *photo = [photos firstObject];
        NSString *picture = [photo objectForKey:@"picture"];
        NSURL *urlTemp = [NSURL URLWithString:picture];
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:urlTemp] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *d, NSError *e){
            cell.imageView.image = [UIImage imageWithData:d];
            cell.imageView.hidden = NO;
        }];
    }else{
        [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"/%@",[dicAlbum objectForKey:@"cover_photo"]] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *con, NSDictionary *result, NSError *e){
            NSString *picture = [result objectForKey:@"picture"];
            NSURL *urlTemp = [NSURL URLWithString:picture];
            [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:urlTemp] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *d, NSError *e){
                cell.imageView.image = [UIImage imageWithData:d];
                cell.imageView.hidden = NO;
            }];
        }];
    }
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSDictionary *dicAlbum = [arrAlbums objectAtIndex:indexPath.row];
    PhotoFBCollectionViewController *destViewCont = [segue destinationViewController];
    if([dicAlbum isEqual:[arrAlbums lastObject]]){
        destViewCont.albumId = @"";
        destViewCont.arrPhotos = [NSMutableArray arrayWithArray:[dicAlbum objectForKey:@"photos"]];
    }else{
        destViewCont.albumId = [dicAlbum objectForKey:@"id"];
        destViewCont.arrPhotos = [NSMutableArray arrayWithCapacity:0];
    }
}
@end