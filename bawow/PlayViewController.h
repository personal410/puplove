//
//  PlayViewController.h
//  bawow
//
//  Created by victor salazar on 8/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import "UsuarioViewController.h"
#import "ProfileDetailViewController.h"
#import "PlayTransitionDelegate.h"
#import "CustomViewController.h"
@class PlayTransitionDelegate;
@interface PlayViewController:CustomViewController<UINavigationControllerDelegate,CLLocationManagerDelegate>{
    BOOL shouldLoadMore, didChangeSearchDistance, requestingMoreProfiles, didObtainLocation, isActive;
    int indArrProfiles;
    CGPoint initialPoint;
    CLLocationManager *locManager;
    NSMutableArray *arrProfiles;
    UIView *tempView;
    User *userJustMatched;
}
@property(nonatomic,weak) IBOutlet UIView *containerImgView;
@property(nonatomic,weak) IBOutlet UIImageView *profileImgView;
@property(nonatomic,weak) IBOutlet UILabel *profileNamelbl;
@property(nonatomic,weak) IBOutlet UIButton *noBtn;
@property(nonatomic,weak) IBOutlet UIButton *yesBtn;

@property(nonatomic,weak) IBOutlet UIView *loadingView;
@property(nonatomic,weak) IBOutlet UILabel *loadingLbl;
@property(nonatomic,weak) IBOutlet UIImageView *loadingImageView;
@end