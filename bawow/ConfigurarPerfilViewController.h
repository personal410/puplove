//
//  ConfigurarPerfilViewController.h
//  bawow
//
//  Created by victor salazar on 26/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UsuarioViewController.h"
#import "ConfPerfilTableViewCell.h"
@interface ConfigurarPerfilViewController:UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>{
    NSArray *breeds;
    UIPickerView *pickerView;
    NSArray *arrTxtFlds;
    User *currentUser;
}
@property(nonatomic) BOOL uploadPhotos;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@end