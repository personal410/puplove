//
//  FriendTableViewController.h
//  bawow
//
//  Created by victor salazar on 14/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MessageController.h"
#import "MZFayeClient.h"
@interface ChatViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate, MZFayeClientDelegate>{
    NSMutableArray *arrMensajes;
    int cantMensajesActual, indexSelectedCell, indPage;
    float totalHeight;
    BOOL loadMoreMessages, showingProfile, didEnterToViewDidLayout;
    MZFayeClient *fayeClient;
    NSString *strChannel;
    User *userMe;
    UIRefreshControl *refCtrl;
    NSMutableDictionary *dicProfileToSend;
}
@property(nonatomic,weak) IBOutlet UIImageView *backgroundImgView;
@property(nonatomic,weak) IBOutlet UITableView *messagesTableView;
@property(nonatomic,weak) IBOutlet UIView *bottomView;
@property(nonatomic,weak) IBOutlet UITextField *messageTxtFld;
@property(nonatomic,weak) IBOutlet UIButton *sendBtn;
@property(nonatomic,weak) IBOutlet UILabel *titleLbl;
@property(nonatomic,strong) User *userFriend;
@end