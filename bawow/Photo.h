//
//  Photo.h
//  bawow
//
//  Created by victor salazar on 17/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Profile;
@interface Photo:NSManagedObject
@property(nonatomic,retain) NSNumber *indPhoto;
@property(nonatomic,retain) NSData *photo;
@property(nonatomic,retain) Profile *profile;
@end