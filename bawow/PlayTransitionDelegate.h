//
//  PlayTransitionDelegate.h
//  bawow
//
//  Created by victor salazar on 23/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PlayViewController.h"
#import "ProfileDetailViewController.h"
@interface PlayTransitionDelegate:NSObject<UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate>{
    BOOL isPresenting;
    float firstScale;
}
@end