//
//  MessageController.h
//  bawow
//
//  Created by victor salazar on 30/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Message.h"
@interface MessageController:NSObject
+(Message *)addMessageWithMessageId:(NSString *)messageId withSender:(User *)sender withReciever:(User *)reciever withMessage:(NSString *)message;
+(Message *)addMessageWithMessageId:(NSString *)messageId withSender:(User *)sender withReciever:(User *)reciever withMessage:(NSString *)message withDate:(NSDate *)date;
+(NSArray *)getConversationWithUser:(User *)user withOffset:(int)offset withCount:(int)count;
@end