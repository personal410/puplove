//
//  IniciarSesionViewController.m
//  bawow
//
//  Created by victor salazar on 1/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "IniciarSesionViewController.h"
@implementation IniciarSesionViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.loginView.readPermissions = @[@"public_profile", @"email", @"user_photos"];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIView animateWithDuration:0.75 animations:^{
        self.loginView.alpha = 1.0;
        self.dogImgView.transform = CGAffineTransformMakeTranslation(0, -10);
        self.titleImgView.transform = CGAffineTransformMakeTranslation(0, (self.view.frame.size.height / 2.0) - 50);
    }];
}
-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    NSString *fbUserId = [user objectID];
    NSString *email = [user objectForKey:@"email"];
    NSDictionary *dicParams = @{@"email": email};
    NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlSignIn] withParams:[ToolBox dictionaryToPostParams:dicParams]];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *dSignIn, NSError *erSignIn){
        if(erSignIn == nil){
            NSError *jsonError = nil;
            id resultSignIn = [NSJSONSerialization JSONObjectWithData:dSignIn options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError == nil){
                NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                if([resultSignIn isKindOfClass:[NSDecimalNumber class]]){
                    if([resultSignIn isEqualToNumber:@404]){
                        NSString *firstName = [user first_name];
                        NSString *lastName = [user last_name];
                        NSDictionary *dicParams = @{@"email": email, @"first_name": firstName, @"last_name": lastName, @"gender": [user objectForKey:@"gender"], @"fb_user_id": fbUserId};
                        NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlUserWithUserId:@""] withParams:[ToolBox dictionaryToPostParams:dicParams]];
                        [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *dSignUp, NSError *eSignUp){
                            [SVProgressHUD dismiss];
                            if(eSignUp == nil){
                                NSError *jsonError = nil;
                                id resultSingUp = [NSJSONSerialization JSONObjectWithData:dSignUp options:NSJSONReadingAllowFragments error:&jsonError];
                                if(jsonError == nil){
                                    NSDictionary *dicUser = [resultSingUp objectForKey:@"user"];
                                    User *userTemp = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                                    userTemp.fbUserId = fbUserId;
                                    userTemp.userId = [dicUser objectForKey:@"id"];
                                    userTemp.firstName = firstName;
                                    userTemp.lastName = lastName;
                                    userTemp.email = email;
                                    userTemp.me = [NSNumber numberWithBool:true];
                                    NSError *error = nil;
                                    [context save:&error];
                                    if(error == nil){
                                        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
                                        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
                                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                                        [self performSegueWithIdentifier:@"mostrarConfPerfil" sender:self];
                                    }else{
                                        [[FBSession activeSession] closeAndClearTokenInformation];
                                        [ToolBox showGenericErrorInViewCont:self];
                                    }
                                }else{
                                    [[FBSession activeSession] closeAndClearTokenInformation];
                                    [ToolBox showGenericErrorInViewCont:self];
                                }
                            }else{
                                [[FBSession activeSession] closeAndClearTokenInformation];
                                NSLog(@"%s error: %@", __FUNCTION__, eSignUp);
                                [ToolBox showConnectionErrorInViewCont:self];
                            }
                        }];
                    }else{
                        [[FBSession activeSession] closeAndClearTokenInformation];
                        [SVProgressHUD dismiss];
                        [ToolBox showGenericErrorInViewCont:self];
                    }
                }else{
                    NSDictionary *dicUser = [resultSignIn objectForKey:@"user"];
                    User *userTemp = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                    userTemp.fbUserId = fbUserId;
                    userTemp.userId = [dicUser objectForKey:@"id"];
                    userTemp.firstName = [dicUser objectForKey:@"first_name"];
                    userTemp.lastName = [dicUser objectForKey:@"last_name"];
                    userTemp.email = email;
                    userTemp.me = [NSNumber numberWithBool:true];
                    id dicProfile = [dicUser objectForKey:@"profile"];
                    NSString *strId = @"";
                    if(dicProfile != [NSNull null]){
                        strId = @"mostrarUsuario";
                        Profile *profile = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
                        profile.profileId = [dicProfile objectForKey:@"id"];
                        profile.name = [dicProfile objectForKey:@"name"];
                        profile.breed = [dicProfile objectForKey:@"breed"];
                        profile.pedigri = [NSNumber numberWithInt:[[dicProfile objectForKey:@"pedigri"] intValue]];
                        profile.gender = [dicProfile objectForKey:@"gender"];
                        NSString *strBirthday = [dicProfile objectForKey:@"birthday"];
                        NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
                        dateFormatter.dateFormat = @"MM/dd/yyyy";
                        profile.birthday = [dateFormatter dateFromString:strBirthday];
                        id strDescrip = [dicProfile objectForKey:@"description"];
                        if(![strDescrip isEqual:[NSNull null]]){
                            profile.descripcion = strDescrip;
                        }
                        NSDictionary *dicPictures = [dicProfile objectForKey:@"pictures"];
                        for(int i = 1; i < 7; i++){
                            NSString *strPictureTemp = [NSString stringWithFormat:@"picture_%i", i];
                            id strUrlPicture = [[[dicPictures objectForKey:strPictureTemp] objectForKey:strPictureTemp] objectForKey:@"url"];
                            if(strUrlPicture != [NSNull null]){
                                NSURL *urlPicture = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [URLs urlIp], strUrlPicture]];
                                NSData *dPicture = [NSData dataWithContentsOfURL:urlPicture];
                                Photo *photoTemp = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
                                photoTemp.indPhoto = @(i);
                                photoTemp.photo = dPicture;
                                photoTemp.profile = profile;
                            }
                        }
                        userTemp.profile = profile;
                    }else{
                        strId = @"mostrarConfPerfil";
                    }
                    NSError *erTemp = nil;
                    [context save:&erTemp];
                    [SVProgressHUD dismiss];
                    if(erTemp == nil){
                        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
                        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                        [self performSegueWithIdentifier:strId sender:self];
                    }else{
                        [[FBSession activeSession] closeAndClearTokenInformation];
                        [ToolBox showGenericErrorInViewCont:self];
                    }
                }
            }else{
                NSString *strJson = [[NSString alloc] initWithData:dSignIn encoding:NSUTF8StringEncoding];
                NSLog(@"strJson: %@", strJson);
                [[FBSession activeSession] closeAndClearTokenInformation];
                [SVProgressHUD dismiss];
                [ToolBox showGenericErrorInViewCont:self];
            }
        }else{
            [[FBSession activeSession] closeAndClearTokenInformation];
            [SVProgressHUD dismiss];
            [ToolBox showConnectionErrorInViewCont:self];
        }
    }];
}
@end
