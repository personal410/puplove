//
//  AppDelegate.h
//  bawow
//
//  Created by victor salazar on 30/12/14.
//  Copyright (c) 2014 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@interface AppDelegate:UIResponder<UIApplicationDelegate>
@property(nonatomic,strong) UIWindow *window;
@property(nonatomic,strong,readonly) NSManagedObjectContext *managedObjectContext;
@property(nonatomic,strong,readonly) NSManagedObjectModel *managedObjectModel;
@property(nonatomic,strong,readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end