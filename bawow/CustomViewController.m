//
//  CustomViewController.m
//  bawow
//
//  Created by victor salazar on 29/08/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "CustomViewController.h"
#import "UsuarioViewController.h"
@implementation CustomViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    UIImage *img = [[UIImage imageNamed:@"Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:img style:UIBarButtonItemStyleDone target:self action:@selector(showOrHideMenu)];
    self.navigationItem.leftBarButtonItem = barButton;
}
-(void)showOrHideMenu{
    UsuarioViewController *usuViewContTemp = (UsuarioViewController *)(self.parentViewController.parentViewController);
    [usuViewContTemp showOrHideMenu];
}
@end
