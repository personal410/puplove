//
//  ResultSearchBreedsTableViewController.h
//  bawow
//
//  Created by victor salazar on 29/07/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ResultSearchBreedsTableViewController:UITableViewController
@property(nonatomic,strong) NSArray *arrResult;
@property(nonatomic) int indSeleccionado;
@end