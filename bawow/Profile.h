//
//  Profile.h
//  bawow
//
//  Created by victor salazar on 25/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Photo, User;
@interface Profile:NSManagedObject
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * breed;
@property (nonatomic, retain) NSString * descripcion;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * pedigri;
@property (nonatomic, retain) NSString * profileId;
@property (nonatomic, retain) NSSet *photos;
@property (nonatomic, retain) User *user;
-(NSMutableDictionary *)dictionary;
@end