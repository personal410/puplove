//
//  MessageTableViewCell.m
//  bawow
//
//  Created by victor salazar on 3/04/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "MessageTableViewCell.h"
#define BlueNormalColor [UIColor colorWithRed:42/255.0 green:145/255.0 blue:252/255.0 alpha:1.0]
#define BlueHighlightColor [UIColor colorWithRed:13/255.0 green:107/255.0 blue:254/255.0 alpha:1.0]
#define GrayNormalColor [UIColor colorWithRed:223/255.0 green:222/255.0 blue:228/255.0 alpha:1.0]
#define GrayHighlightColor [UIColor colorWithRed:202/255.0 green:201/255.0 blue:201/255.0 alpha:1.0]
@implementation MessageTableViewCell
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        self.topTypeCorner = NormalCorner;
        self.bottomTypeCorner = NormalCorner;
        self.friendBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    }
    return self;
}
-(void)drawCell{
    if(roundedView){
        [roundedView removeFromSuperview];
    }
    if(messageLbl){
        [messageLbl removeFromSuperview];
    }
    if(friendImgView){
        [friendImgView removeFromSuperview];
    }
    roundedView = [[UIView alloc] initWithFrame:self.viewFrame];
    roundedView.tag = 1;
    
    [self changeLayerWithHightlight:false];
    
    CGRect lblFrame = CGRectInset(roundedView.frame, 12, 10);
    messageLbl = [[UILabel alloc] initWithFrame:lblFrame];
    messageLbl.lineBreakMode = NSLineBreakByWordWrapping;
    messageLbl.adjustsFontSizeToFitWidth = false;
    messageLbl.textColor = (self.side == RightSide)?[UIColor whiteColor]:[UIColor blackColor];
    messageLbl.numberOfLines = 0;
    messageLbl.text = self.strMessage;
    if(self.friendImage){
        friendImgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, self.frame.size.height - 37, 35, 35)];
        friendImgView.layer.cornerRadius = 17.5;
        friendImgView.clipsToBounds = true;
        friendImgView.image = self.friendImage;
        [self addSubview:friendImgView];
        self.friendBtn.frame = friendImgView.frame;
    }else{
        self.friendBtn.frame = CGRectZero;
    }
    [self addSubview:roundedView];
    [self addSubview:messageLbl];
}
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    if(self.friendImage){
        if(CGRectContainsPoint(self.friendBtn.frame, point)){
            [self.friendBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
            return false;
        }
    }
    return CGRectContainsPoint(roundedView.frame, point);
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    [self changeLayerWithHightlight:highlighted];
}
-(void)changeLayerWithHightlight:(BOOL)highlight{
    float topRightCornerRadius = (self.side == RightSide)?((self.topTypeCorner == NormalCorner)?SizeNormalCorner:SizeTinyCorner):SizeNormalCorner;
    float topLeftCornerRadius = (self.side == LeftSide)?((self.topTypeCorner == NormalCorner)?SizeNormalCorner:SizeTinyCorner):SizeNormalCorner;
    float bottomRightCornerRadius = (self.side == RightSide)?((self.bottomTypeCorner == NormalCorner)?SizeNormalCorner:SizeTinyCorner):SizeNormalCorner;
    float bottomLeftCornerRadius = (self.side == LeftSide)?((self.bottomTypeCorner == NormalCorner)?SizeNormalCorner:SizeTinyCorner):SizeNormalCorner;
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(topLeftCornerRadius, 0)];
    [bezierPath addLineToPoint:CGPointMake(roundedView.frame.size.width - topRightCornerRadius, 0)];
    [bezierPath addArcWithCenter:CGPointMake(roundedView.frame.size.width - topRightCornerRadius, topRightCornerRadius) radius:topRightCornerRadius startAngle:M_PI_2 endAngle:0 clockwise:true];
    [bezierPath addLineToPoint:CGPointMake(roundedView.frame.size.width, roundedView.frame.size.height - bottomRightCornerRadius)];
    [bezierPath addArcWithCenter:CGPointMake(roundedView.frame.size.width - bottomRightCornerRadius, roundedView.frame.size.height - bottomRightCornerRadius) radius:bottomRightCornerRadius startAngle:0 endAngle:M_PI_2 clockwise:true];
    [bezierPath addLineToPoint:CGPointMake(bottomLeftCornerRadius, roundedView.frame.size.height)];
    [bezierPath addArcWithCenter:CGPointMake(bottomLeftCornerRadius, roundedView.frame.size.height - bottomLeftCornerRadius) radius:bottomLeftCornerRadius startAngle:1.5*M_PI endAngle:M_PI clockwise:true];
    [bezierPath addLineToPoint:CGPointMake(0, topLeftCornerRadius)];
    [bezierPath addArcWithCenter:CGPointMake(topLeftCornerRadius, topLeftCornerRadius) radius:topLeftCornerRadius startAngle:M_PI endAngle:M_PI_2 clockwise:true];
    CAShapeLayer *backShapeLayer = [CAShapeLayer layer];
    backShapeLayer.path = bezierPath.CGPath;
    UIColor *grayColor = (highlight)?GrayHighlightColor:GrayNormalColor;
    UIColor *blueColor = (highlight)?BlueHighlightColor:BlueNormalColor;
    backShapeLayer.fillColor = [((self.side == LeftSide)?grayColor:blueColor) CGColor];
    [roundedView.layer addSublayer:backShapeLayer];
}
@end