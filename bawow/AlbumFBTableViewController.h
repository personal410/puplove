//
//  AlbumTableViewController.h
//  bawow
//
//  Created by victor salazar on 28/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Diccionario.h"
#import "PhotoFBCollectionViewController.h"
@interface AlbumFBTableViewController:UITableViewController{
    NSMutableArray *arrAlbums, *arrPhotosOfMe;
}
@end