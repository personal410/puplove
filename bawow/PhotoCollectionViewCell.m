//
//  PhotoCollectionViewCell.m
//  bawow
//
//  Created by victor salazar on 29/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "PhotoCollectionViewCell.h"
@implementation PhotoCollectionViewCell
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    for(CALayer *layerTemp in self.whiteView.layer.sublayers){
        [layerTemp removeFromSuperlayer];
    }
    for(CALayer *layerTemp in self.addOrRemoveView.layer.sublayers){
        [layerTemp removeFromSuperlayer];
    }
    CAShapeLayer *whiteLayer = [CAShapeLayer layer];
    UIBezierPath *whitePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(17, 17) radius:17 startAngle:0 endAngle:2*M_PI clockwise:NO];
    whiteLayer.path = whitePath.CGPath;
    whiteLayer.fillColor = [[UIColor colorWithRed:236/255.0 green:236/255.0 blue:243/255.0 alpha:1.0] CGColor];
    [self.whiteView.layer addSublayer:whiteLayer];
    CAShapeLayer *cornerOrangeShape = [CAShapeLayer layer];
    UIBezierPath *cornerOrangePath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(8.5, 8.5) radius:9 startAngle:0 endAngle:2*M_PI clockwise:NO];
    if(self.photoImgView.image){
        [cornerOrangePath moveToPoint:CGPointMake(9 - 2.25*sqrtf(2), 9 - 2.25*sqrtf(2))];
        [cornerOrangePath addLineToPoint:CGPointMake(9 + 2.25*sqrtf(2), 9 + 2.25*sqrtf(2))];
        [cornerOrangePath moveToPoint:CGPointMake(9 - 2.25*sqrtf(2), 9 + 2.25*sqrtf(2))];
        [cornerOrangePath addLineToPoint:CGPointMake(9 + 2.25*sqrtf(2), 9 - 2.25*sqrtf(2))];
    }else{
        [cornerOrangePath moveToPoint:CGPointMake(3.5, 9)];
        [cornerOrangePath addLineToPoint:CGPointMake(14.5, 9)];
        [cornerOrangePath moveToPoint:CGPointMake(9, 3.5)];
        [cornerOrangePath addLineToPoint:CGPointMake(9, 14.5)];
    }
    cornerOrangeShape.path = cornerOrangePath.CGPath;
    cornerOrangeShape.strokeColor = [[UIColor colorWithRed:231/255.0 green:92/255.0 blue:62/255.0 alpha:1.0] CGColor];
    cornerOrangeShape.fillColor = [[UIColor clearColor] CGColor];
    [self.addOrRemoveView.layer addSublayer:cornerOrangeShape];
}
@end