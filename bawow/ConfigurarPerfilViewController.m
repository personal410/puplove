//
//  ConfigurarPerfilViewController.m
//  bawow
//
//  Created by victor salazar on 26/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ConfigurarPerfilViewController.h"
@implementation ConfigurarPerfilViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    breeds = [[Diccionario sharedDiccionario] arrRazas];
    UIBarButtonItem *barItemTemp = [[UIBarButtonItem alloc] initWithTitle:[[Diccionario sharedDiccionario] stringGuardar] style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    self.navigationItem.rightBarButtonItem = barItemTemp;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:[[Diccionario sharedDiccionario] stringAtras] style:UIBarButtonItemStyleDone target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    if([[[[UserController getUser] profile] profileId] length] > 0){
        UIBarButtonItem *cancelBarItem = [[UIBarButtonItem alloc] initWithTitle:[[Diccionario sharedDiccionario] stringCancelar] style:UIBarButtonItemStyleDone target:self action:@selector(cancel)];
        self.navigationItem.leftBarButtonItem = cancelBarItem;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    currentUser = [UserController getUser];
    Profile *profile = [currentUser profile];
    NSSet *photos = profile.photos;
    if(photos.count > 0){
        for(Photo *photoTemp in photos){
            if([photoTemp.indPhoto isEqualToNumber:@1]){
                cell.profileImgView.image = [UIImage imageWithData:photoTemp.photo];
                break;
            }
        }
    }
    cell.breedTxtFld.text = currentUser.profile.breed;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if(cell.nameTxtFld.isFirstResponder){
        [cell.nameTxtFld resignFirstResponder];
    }else if (cell.birthTxtFld.isFirstResponder){
        [cell.birthTxtFld resignFirstResponder];
    }else if (cell.descripcionTxtView.isFirstResponder){
        [cell.descripcionTxtView resignFirstResponder];
    }
    [self hideKeyboard:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showBreeds"]){
        ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        UIViewController *viewCont = segue.destinationViewController;
        [viewCont setValue:cell.breedTxtFld.text forKey:@"currentBreed"];
        [viewCont setValue:currentUser forKey:@"currentUser"];
    }
}
#pragma mark - Table View Events
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ConfPerfilTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    User *user = [UserController getUser];
    Profile *profile = [user profile];
    cell.profileImgView.layer.cornerRadius = cell.profileImgView.frame.size.width/2.0;
    NSSet *photos = profile.photos;
    if(photos.count > 0){
        for(Photo *photoTemp in photos){
            if([photoTemp.indPhoto isEqualToNumber:@1]){
                cell.profileImgView.image = [UIImage imageWithData:photoTemp.photo];
                break;
            }
        }
    }
    cell.nameTxtFld.text = profile.name;
    cell.breedTxtFld.text = profile.breed;
    pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    cell.breedTxtFld.inputView = pickerView;
    if(profile.breed.length > 0){
        int indBreed = (int)[breeds indexOfObject:profile.breed];
        [pickerView selectRow:indBreed inComponent:0 animated:NO];
    }
    if(profile.profileId.length > 0){
        cell.pedrigiSwitch.on = [profile.pedigri boolValue];
    }
    NSArray *arrOpcionesSegCtrl = [[Diccionario sharedDiccionario] arrOpcionesSegCtrlConfPer];
    [cell.genderSegCtrl setTitle:[arrOpcionesSegCtrl objectAtIndex:0] forSegmentAtIndex:0];
    [cell.genderSegCtrl setTitle:[arrOpcionesSegCtrl objectAtIndex:1] forSegmentAtIndex:1];
    if(profile.gender.length > 0){
        int indGender = (int)[[[Diccionario sharedDiccionario] arrValoresOpcSegCtrlGender] indexOfObject:profile.gender];
        cell.genderSegCtrl.selectedSegmentIndex = indGender;
    }
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    NSDate *today = [NSDate date];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = -1;
    NSDate *maxDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:today options:0];
    datePicker.maximumDate = maxDate;
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerChangeDate:) forControlEvents:UIControlEventValueChanged];
    cell.birthTxtFld.inputView = datePicker;
    if(profile.birthday){
        NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
        dateFormatter.dateFormat = @"dd MMMM yyyy";
        cell.birthTxtFld.text = [dateFormatter stringFromDate:profile.birthday];
        datePicker.date = profile.birthday;
    }
    cell.descripcionTxtView.layer.borderWidth = 1;
    cell.descripcionTxtView.layer.borderColor = [[UIColor colorWithRed:196/255.0 green:196/255.0 blue:196/255.0 alpha:1.0] CGColor];
    cell.descripcionTxtView.layer.cornerRadius = 8.f;
    cell.descripcionTxtView.text = profile.descripcion;
    return cell;
}
-(void)datePickerChangeDate:(UIDatePicker *)datePicker1{
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    [cell.birthTxtFld setText:[dateFormatter stringFromDate:datePicker1.date]];
}
-(void)previous:(id)sender{
    int tag = (int)[sender tag];
    tag--;
    UITextField *nextField = [arrTxtFlds objectAtIndex:tag];
    [nextField becomeFirstResponder];
}
-(void)next:(id)sender{
    int tag = (int)[sender tag];
    tag++;
    UITextField *nextField = [arrTxtFlds objectAtIndex:tag];
    [nextField becomeFirstResponder];
}
-(void)done:(id)sender{
    int tag = (int)[sender tag];
    UITextField *nextField = [arrTxtFlds objectAtIndex:tag];
    [nextField resignFirstResponder];
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
}
#pragma mark - TextField Events
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if([cell.breedTxtFld isEqual:textField]){
        [self performSegueWithIdentifier:@"showBreeds" sender:self];
        return NO;
    }else{
        [self.tableView setContentOffset:CGPointMake(0, 40 + (textField.tag - 1)*40) animated:YES];
    }
    return YES;
}
#pragma makr - TextViewEvents
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@"Description"]){
        textView.text = @"";
    }
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if([textView.text isEqualToString:@""]){
        textView.text = @"Description";
    }
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return breeds.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [breeds objectAtIndex:row];
}
-(void)showOrHideMenu{
    UsuarioViewController *usuViewContTemp = (UsuarioViewController *)(self.parentViewController.parentViewController);
    [usuViewContTemp showOrHideMenu];
}
-(void)save{
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if(cell.nameTxtFld.isFirstResponder){
        [cell.nameTxtFld resignFirstResponder];
    }else if (cell.breedTxtFld.isFirstResponder){
        [cell.breedTxtFld resignFirstResponder];
    }else if (cell.birthTxtFld.isFirstResponder){
        [cell.birthTxtFld resignFirstResponder];
    }else if (cell.descripcionTxtView.isFirstResponder){
        [cell.descripcionTxtView resignFirstResponder];
    }
    User *userActual = [UserController getUser];
    if(userActual.profile.photos.count > 0){
        NSString *name = cell.nameTxtFld.text;
        NSString *breed = cell.breedTxtFld.text;
        NSNumber *pedigri = [NSNumber numberWithBool:cell.pedrigiSwitch.on];
        int indGender = (int)cell.genderSegCtrl.selectedSegmentIndex;
        NSString *gender = [[[Diccionario sharedDiccionario] arrValoresOpcSegCtrlGender] objectAtIndex:indGender];
        NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
        dateFormatter.dateFormat = @"MM/dd/yyyy";
        UIDatePicker *datePicker1 = (UIDatePicker *)cell.birthTxtFld.inputView;
        NSString *birthday = [dateFormatter stringFromDate:datePicker1.date];
        NSString *descripcion = cell.descripcionTxtView.text;
        if((name.length > 0) && (breed.length > 0) && (cell.birthTxtFld.text.length > 0) && ![descripcion isEqual:@"Description"]){
            NSMutableDictionary *dicParams = [NSMutableDictionary dictionaryWithDictionary:@{@"name": name, @"breed": breed, @"gender": gender, @"pedigri": pedigri, @"birthday": birthday, @"description": descripcion}];
            if([[Diccionario sharedDiccionario] uploadPhotos]){
                for(Photo *photo in userActual.profile.photos){
                    NSNumber *indPhoto = photo.indPhoto;
                    NSData *dTemp = photo.photo;
                    NSString *base64Temp = [dTemp base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                    base64Temp = [base64Temp stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
                    [dicParams setObject:base64Temp forKey:[NSString stringWithFormat:@"picture_%@", indPhoto]];
                }
            }
            NSString *profileIdTemp = (userActual.profile.profileId)?(userActual.profile.profileId):@"";
            NSString *strUrlTemp = [URLs urlCreateProfileWithUserId:userActual.userId withProfileId:profileIdTemp];
            NSString *strMethod = (profileIdTemp.length == 0)?@"POST":@"PUT";
            NSURLRequest *req = [ToolBox createRequestWithUrl:strUrlTemp withParams:[ToolBox dictionaryToPostParams:dicParams] withMethod:strMethod];
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *data, NSError *error){
                [SVProgressHUD dismiss];
                if(error == nil){
                    NSError *jsonError = nil;
                    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                    if(jsonError == nil){
                        Profile *profile = userActual.profile;
                        profile.name = name;
                        profile.breed = breed;
                        profile.pedigri = pedigri;
                        profile.gender = gender;
                        profile.birthday = datePicker1.date;
                        profile.descripcion = descripcion;
                        if(profileIdTemp.length == 0){
                            NSDictionary *dicProfile = [result objectForKey:@"profile"];
                            profile.profileId = [dicProfile objectForKey:@"id"];
                        }
                        NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                        userActual.profile = profile;
                        NSError *erTemp = nil;
                        [context save:&erTemp];
                        if(erTemp == nil){
                            if([[Diccionario sharedDiccionario] uploadPhotos]){
                                NSArray *arrViewConts = [self.parentViewController.parentViewController childViewControllers];
                                for(id viewCont in arrViewConts){
                                    if([viewCont isKindOfClass:[MenuViewController class]]){
                                        [viewCont actualizarFotoPerfil];
                                        break;
                                    }
                                }
                            }
                            [[Diccionario sharedDiccionario] setUploadPhotos:NO];
                            if([self.presentingViewController isKindOfClass:[UsuarioViewController class]]){
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }else{
                                [self performSegueWithIdentifier:@"mostrarUsuario" sender:self];
                            }
                        }else{
                            [ToolBox showGenericErrorInViewCont:self];
                        }
                    }else{
                        [ToolBox showGenericErrorInViewCont:self];
                    }
                }else{
                    NSLog(@"%s error: %@", __FUNCTION__, error);
                    [ToolBox showConnectionErrorInViewCont:self];
                }
            }];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:[[Diccionario sharedDiccionario] stringAlerta] message:@"Fill all the fields." preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertCont animated:true completion:nil];
        }
    }else{
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:[[Diccionario sharedDiccionario] stringAlerta] message:[[Diccionario sharedDiccionario] stringSinFotos] preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertCont animated:true completion:nil];
    }
}
-(void)cancel{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    [context rollback];
    [self dismissViewControllerAnimated:true completion:nil];
}
-(IBAction)actionUploadPhotos:(id)sender{
    [self performSegueWithIdentifier:@"showPhotos" sender:self];
}
#pragma mark - UIKeyboard
-(void)showKeyboard:(NSNotification *)notification{
    NSValue *value = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rect = [value CGRectValue];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
}
-(void)hideKeyboard:(id)sender{
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}
-(IBAction)dismissKeyboard:(id)sender{
    ConfPerfilTableViewCell *cell = (ConfPerfilTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.nameTxtFld resignFirstResponder];
    [cell.birthTxtFld resignFirstResponder];
    [cell.descripcionTxtView resignFirstResponder];
}
@end