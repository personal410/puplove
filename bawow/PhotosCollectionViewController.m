//
//  PhotosCollectionViewController.m
//  bawow
//
//  Created by victor salazar on 27/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "PhotosCollectionViewController.h"
@implementation PhotosCollectionViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
    self.navigationItem.title = [[Diccionario sharedDiccionario] stringFotos];
    User *usuario = [UserController getUser];
    photos = usuario.profile.photos;
}
#pragma mark <UICollectionViewDataSource>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float totalWidth = self.collectionView.frame.size.width;
    float newTotalWidth = totalWidth - 30;
    float widthTemp = newTotalWidth/2.0;
    return CGSizeMake(widthTemp, widthTemp);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 6;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    [cell setNeedsDisplay];
    cell.photoImgView.image = nil;
    int ind = (int)indexPath.row;
    ind++;
    Photo *photo = nil;
    for(Photo *photoTemp in photos){
        if([photoTemp.indPhoto isEqualToNumber:@(ind)]){
            photo = photoTemp;
            break;
        }
    }
    if(photo){
        cell.photoImgView.image = [UIImage imageWithData:photo.photo];
    }
    return cell;
}
#pragma mark <UICollectionViewDelegate>
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    UIAlertAction *actionCancelar = [UIAlertAction actionWithTitle:[[Diccionario sharedDiccionario] stringCancelar] style:UIAlertActionStyleCancel handler:nil];
    if(cell.photoImgView.image){
        UIAlertAction *actionDeletePhoto = [UIAlertAction actionWithTitle:[[Diccionario sharedDiccionario] stringEliminarFoto] style:UIAlertActionStyleDefault handler:^(UIAlertAction *alertAction){
            NSSet *photosTemp = [UserController getUser].profile.photos;
            if(photosTemp.count > 1){
                NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                int ind = (int)indexPath.item + 1;
                for(Photo *p in photosTemp){
                    if([p.indPhoto isEqualToNumber:@(ind)]){
                        [context deleteObject:p];
                        break;
                    }
                }
                for(Photo *p in photosTemp){
                    int indActual = p.indPhoto.intValue;
                    if (indActual > ind) {
                        indActual--;
                    }
                    p.indPhoto = @(indActual);
                }
                [[Diccionario sharedDiccionario] setUploadPhotos:YES];
                [self.collectionView reloadData];
            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[Diccionario sharedDiccionario] stringAlerta] message:@"Must set at least 1 photo." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertTemp = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:alertTemp];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }];
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alertCont addAction:actionDeletePhoto];
        [alertCont addAction:actionCancelar];
        [self presentViewController:alertCont animated:YES completion:nil];
    }else{
        UIAlertAction *alertAction1 = [UIAlertAction actionWithTitle:[[Diccionario sharedDiccionario] stringLibreria] style:UIAlertActionStyleDefault handler:^(UIAlertAction *alertAction){
            UIImagePickerController *pickerCont = [[UIImagePickerController alloc] init];
            pickerCont.delegate = self;
            pickerCont.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:pickerCont animated:YES completion:nil];
        }];
        UIAlertAction *alertAction2 = [UIAlertAction actionWithTitle:@"Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction *alertAction){
            [self performSegueWithIdentifier:@"pickFBPhoto" sender:self];
        }];
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [alertCont addAction:alertAction1];
        [alertCont addAction:alertAction2];
        [alertCont addAction:actionCancelar];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    imageTemp = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"cropPhoto" sender:self];
    }];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"cropPhoto"]){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        CropPhotoViewController *viewCont = segue.destinationViewController;
        viewCont.image = imageTemp;
    }
}
@end