;//
//  MessageController.m
//  bawow
//
//  Created by victor salazar on 30/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "MessageController.h"
@implementation MessageController
+(NSArray *)getConversationWithUser:(User *)user withOffset:(int)offset withCount:(int)count{
    NSMutableArray *arrMessages = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *arrAllMessages = [NSMutableArray arrayWithCapacity:0];
    if(user.sentMessages.count > 0){
        [arrAllMessages addObjectsFromArray:user.sentMessages.allObjects];
    }
    if(user.receivedMessages.count > 0){
        [arrAllMessages addObjectsFromArray:user.receivedMessages.allObjects];
    }
    for(Message *messageTemp in arrAllMessages){
        if([messageTemp isEqual:[arrAllMessages firstObject]]) {
            [arrMessages addObject:messageTemp];
        }else{
            BOOL inserted = false;
            NSDate *dateMessageTemp = messageTemp.date;
            for(int i = 0; i < arrMessages.count; i++){
                Message *fMessage = [arrMessages objectAtIndex:i];
                NSDate *dateFinalMessage = [fMessage date];
                if([dateMessageTemp compare:dateFinalMessage] == NSOrderedDescending){
                    inserted = true;
                    [arrMessages insertObject:messageTemp atIndex:i];
                    break;
                }
            }
            if(!inserted){
                [arrMessages addObject:messageTemp];
            }
        }
    }
    NSMutableArray *arrToReturn = [NSMutableArray array];
    int realCount = MIN((int)arrMessages.count, offset*15 + count);
    if(arrMessages.count > 0){
        for(int i = offset*15; i < realCount; i++){
            [arrToReturn addObject:[arrMessages objectAtIndex:i]];
        }
    }
    return arrToReturn;
}
+(Message *)addMessageWithMessageId:(NSString *)messageId withSender:(User *)sender withReciever:(User *)reciever withMessage:(NSString *)message{
    return [self addMessageWithMessageId:messageId withSender:sender withReciever:reciever withMessage:message withDate:[NSDate date]];
}
+(Message *)addMessageWithMessageId:(NSString *)messageId withSender:(User *)sender withReciever:(User *)reciever withMessage:(NSString *)message withDate:(NSDate *)date{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    Message *mesTemp = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Message class]) inManagedObjectContext:context];
    mesTemp.messageId = messageId;
    mesTemp.message = message;
    mesTemp.sender = sender;
    mesTemp.reciever = reciever;
    mesTemp.date = date;
    NSError *errorSaveMessage = nil;
    [context save:&errorSaveMessage];
    if(errorSaveMessage){
        NSLog(@"errorSaveMessage: %@", errorSaveMessage.description);
        return nil;
    }
    return mesTemp;
}
@end