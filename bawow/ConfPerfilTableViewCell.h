//
//  ConfPerfilTableViewCell.h
//  bawow
//
//  Created by victor salazar on 12/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ConfPerfilTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *profileImgView;
@property(nonatomic,weak) IBOutlet UIButton *updatePhotosBtn;
@property(nonatomic,weak) IBOutlet UITextField *nameTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *breedTxtFld;
@property(nonatomic,weak) IBOutlet UISwitch *pedrigiSwitch;
@property(nonatomic,weak) IBOutlet UISegmentedControl *genderSegCtrl;
@property(nonatomic,weak) IBOutlet UITextField *birthTxtFld;
@property(nonatomic,weak) IBOutlet UITextView *descripcionTxtView;
@end