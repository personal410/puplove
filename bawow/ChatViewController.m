//
//  FriendTableViewController.m
//  bawow
//
//  Created by victor salazar on 14/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ChatViewController.h"
#import "MessageTableViewCell.h"
#import "SentDateTableViewCell.h"
#import "ProfileDetailViewController.h"
#define cellPadding 2.0
@implementation ChatViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    userMe = [UserController getUser];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    loadMoreMessages = true;
    indexSelectedCell = -1;
    arrMensajes = [NSMutableArray new];
    [self cargarMensajes:nil];
    [self.sendBtn setTitle:[[Diccionario sharedDiccionario] strEnviar] forState:UIControlStateNormal];
    self.titleLbl.text = [NSString stringWithFormat:@"%@ %@", self.userFriend.firstName, self.userFriend.lastName];
    refCtrl = [UIRefreshControl new];
    [refCtrl addTarget:self action:@selector(cargarMensajes:) forControlEvents:UIControlEventValueChanged];
    [self.messagesTableView addSubview:refCtrl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectToChannel) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectToChannel) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self setTotalHeight];
    self.messagesTableView.contentSize = CGSizeMake(self.messagesTableView.frame.size.width, totalHeight);
    
    [UserController updateUser:self.userFriend withDictionary:@{@"lastMessage": @""}];
    
    strChannel = [NSString stringWithFormat:@"/chat/%@", self.userFriend.channel];
    fayeClient = [[MZFayeClient alloc] initWithURL:[NSURL URLWithString:@"ws://core.puplove.club/faye"]];
    [fayeClient subscribeToChannel:strChannel];
    fayeClient.delegate = self;
    [self connectToChannel];
    Message *lastMesasge = arrMensajes.firstObject;
    NSMutableDictionary *dicParams = [NSMutableDictionary dictionaryWithDictionary:@{@"recipient_id": self.userFriend.userId}];
    if(lastMesasge){
        [dicParams setObject:lastMesasge.messageId forKey:@"message_id"];
    }
    NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlRetrieveMessagesWithUserId:userMe.userId] withParams:[ToolBox dictionaryToPostParams:dicParams]];
    [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *data, NSError *error) {
        if(error){
            NSLog(@"%s error: %@", __FUNCTION__, error);
            [ToolBox showConnectionErrorInViewCont:self];
        }else{
            NSError *jsonError;
            NSArray *arrDicMessages = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                [ToolBox showGenericErrorInViewCont:self];
            }else{
                if(arrDicMessages.count > 0){
                    NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
                    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
                    NSMutableArray *arrIndexPaths = [NSMutableArray array];
                    for(NSDictionary *dicMessage in arrDicMessages){
                        NSDictionary *dicInfoMessage = [dicMessage objectForKey:@"message"];
                        NSString *strCreatedAt = [dicInfoMessage objectForKey:@"created_at"];
                        NSDate *dateCreatedAt = [dateFormatter dateFromString:strCreatedAt];
                        BOOL isSender = [[dicInfoMessage objectForKey:@"sender"] boolValue];
                        Message *mensaje = [MessageController addMessageWithMessageId:[dicInfoMessage objectForKey:@"id"] withSender:(isSender?userMe:self.userFriend) withReciever:(isSender?self.userFriend:userMe) withMessage:[dicInfoMessage objectForKey:@"text"] withDate:dateCreatedAt];
                        [arrIndexPaths addObject:[NSIndexPath indexPathForRow:([arrDicMessages indexOfObject:dicMessage] + arrMensajes.count) inSection:0]];
                        [self setHeightWithMensaje:mensaje];
                    }
                    NSArray *arrMensajesTemp = [MessageController getConversationWithUser:self.userFriend withOffset:0 withCount:(int)arrDicMessages.count];
                    NSMutableArray *prevArrMessages = arrMensajes;
                    arrMensajes = [NSMutableArray arrayWithArray:arrMensajesTemp];
                    [arrMensajes addObjectsFromArray:prevArrMessages];
                    [self setTotalHeight];
                    [self.messagesTableView beginUpdates];
                    [self.messagesTableView insertRowsAtIndexPaths:arrIndexPaths withRowAnimation:UITableViewRowAnimationTop];
                    [self.messagesTableView endUpdates];
                    if(totalHeight > self.messagesTableView.frame.size.height){
                        [self.messagesTableView setContentOffset:CGPointMake(0, totalHeight - self.messagesTableView.frame.size.height) animated:true];
                    }
                }
            }
        }
    }];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(!didEnterToViewDidLayout){
        didEnterToViewDidLayout = true;
        if(totalHeight > self.messagesTableView.frame.size.height){
            self.messagesTableView.contentOffset = CGPointMake(0, totalHeight - self.messagesTableView.frame.size.height);
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    self.titleLbl.textColor = [UIColor blackColor];
    if([self.messageTxtFld isFirstResponder]){
        [self.messageTxtFld resignFirstResponder];
    }
    if([fayeClient isConnected]){
        [self disconnectToChannel];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ProfileDetailViewController *viewCont = segue.destinationViewController;
    viewCont.dicProfile = dicProfileToSend;
    viewCont.shouldAnimated = false;
    showingProfile = false;
}
#pragma mark - FayeClient
-(void)fayeClient:(MZFayeClient *)client didConnectToURL:(NSURL *)url{
    self.titleLbl.text = @"Connecting";
}
-(void)fayeClient:(MZFayeClient *)client didSubscribeToChannel:(NSString *)channel{
    self.titleLbl.textColor = [UIColor blackColor];
    self.titleLbl.text = [NSString stringWithFormat:@"%@ %@", self.userFriend.firstName, self.userFriend.lastName];
    if(self.messageTxtFld.text.length > 0){
        self.sendBtn.enabled = true;
    }
}
-(void)fayeClient:(MZFayeClient *)client didReceiveMessage:(NSDictionary *)messageData fromChannel:(NSString *)channel{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    User *me = [UserController getUser];
    User *sender = ([me.userId isEqualToString:messageData[@"user_id"]])?me:self.userFriend;
    User *reciever = ([sender isEqual:me])?self.userFriend:me;
    Message *mensaje = [MessageController addMessageWithMessageId:messageData[@"id"] withSender:sender withReciever:reciever withMessage:messageData[@"message"]];
    if(mensaje == nil){
        [ToolBox showAlertWith:@"Alert" and:@"No se pudo guardar el mensaje" inViewCont:self];
    }else{
        [arrMensajes insertObject:mensaje atIndex:0];
        [self setTotalHeight];
        [self.messagesTableView beginUpdates];
        if(arrMensajes.count > 1){
            [self.messagesTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:arrMensajes.count - 2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            if(arrMensajes.count > 2){
                [self.messagesTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:arrMensajes.count - 3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        [self.messagesTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(arrMensajes.count + (indexSelectedCell != -1?1:0) - 1) inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        if(indexSelectedCell > -1){
            indexSelectedCell++;
        }
        [self.messagesTableView endUpdates];
        int tempOffsetY = totalHeight - self.messagesTableView.frame.size.height + self.messagesTableView.contentInset.bottom + ((indexSelectedCell == -1)?0:20);
        if(tempOffsetY < 0){
            tempOffsetY = 0;
        }
        [self.messagesTableView setContentOffset:CGPointMake(0, tempOffsetY) animated:true];
    }
}
-(void)fayeClient:(MZFayeClient *)client didFailDeserializeMessage:(NSDictionary *)message withError:(NSError *)error{
    NSLog(@"didFailDeserializeMessage");
}
-(void)fayeClient:(MZFayeClient *)client didFailWithError:(NSError *)error{
    [ToolBox showGenericErrorInViewCont:self];
    self.titleLbl.text = @"No Connection";
}
#pragma mark - Gesture Recognizer
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if([gestureRecognizer isEqual:self.navigationController.interactivePopGestureRecognizer]){
        return NO;
    }else{
        return true;
    }
}
-(IBAction)evaluateTap:(UITapGestureRecognizer *)tapGesture{
    NSArray *indexPaths = [self.messagesTableView indexPathsForRowsInRect:CGRectMake(0, self.messagesTableView.contentOffset.y, self.messagesTableView.frame.size.width, self.messagesTableView.frame.size.height)];
    for(int i = 0; i < indexPaths.count; i++){
        UITableViewCell *cell = [self.messagesTableView cellForRowAtIndexPath:[indexPaths objectAtIndex:i]];
        CGPoint puntoToque = [tapGesture locationInView:cell];
        if([cell pointInside:puntoToque withEvent:UIEventTypeTouches]){
            [self tableView:self.messagesTableView didSelectRowAtIndexPath:[indexPaths objectAtIndex:i]];
            return;
        }
    }
    if([self.messageTxtFld isFirstResponder]){
        [self.messageTxtFld resignFirstResponder];
    }
}
#pragma mark - UITextField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.sendBtn.enabled = (newString.length > 0) && fayeClient.isConnected && (fayeClient.openSubscriptions.allObjects.count > 0);
    return YES;
}
#pragma mark - UIKeyboard
-(void)showKeyboard:(NSNotification *)notification{
    NSValue *value = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect rect = [value CGRectValue];
    self.messagesTableView.contentInset = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
    self.messagesTableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
    BOOL shouldReloadContentOffset = self.messagesTableView.contentSize.height > self.messagesTableView.frame.size.height - rect.size.height;
    [UIView animateWithDuration:[UIKeyboardAnimationDurationUserInfoKey doubleValue] delay:0.0 options:kNilOptions animations:^{
        self.bottomView.transform = CGAffineTransformMakeTranslation(0, -rect.size.height);
        if(shouldReloadContentOffset){
            float bottomSpace = self.messagesTableView.contentSize.height - self.messagesTableView.frame.size.height - self.messagesTableView.contentOffset.y;
            if(bottomSpace < 0){
                bottomSpace = 0;
            }
            float newOffset = self.messagesTableView.contentSize.height - (self.messagesTableView.frame.size.height - rect.size.height) - bottomSpace;
            self.messagesTableView.contentOffset = CGPointMake(0, newOffset);
        }
    } completion:nil];
}
-(void)hideKeyboard:(id)sender{
    float previousContentInsetBottom = self.messagesTableView.contentInset.bottom;
    float yOffset = self.messagesTableView.contentOffset.y;
    self.messagesTableView.contentInset = UIEdgeInsetsZero;
    self.messagesTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    [UIView animateWithDuration:[UIKeyboardAnimationDurationUserInfoKey doubleValue] delay:0.0 options:kNilOptions animations:^{
        self.bottomView.transform = CGAffineTransformIdentity;
        float newYOffset = yOffset - previousContentInsetBottom;
        if(newYOffset < 0){
            newYOffset = 0;
        }
        [self.messagesTableView setContentOffset:CGPointMake(0, newYOffset) animated:true];
    } completion:nil];
}
#pragma mark - Other Events
-(IBAction)sendMessage:(UIButton *)sender{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    [fayeClient sendMessage:@{@"message": self.messageTxtFld.text, @"ext": @{@"channel_token": self.userFriend.token, @"user_id": userMe.userId, @"channel_name": self.userFriend.channel}} toChannel:strChannel];
    [self.messageTxtFld setText:@""];
    self.sendBtn.enabled = NO;
}
-(void)setHeightWithMensaje:(Message *)messageTemp{
    float maxWidth = self.view.frame.size.width - 90.0 - 24.0;
    CGRect frameTemp = [messageTemp.message boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.0]} context:nil];
    frameTemp.origin.x = 12.0;
    frameTemp.origin.y = 10.0;
    frameTemp.size.height = ceilf(frameTemp.size.height);
    [messageTemp setFrame:frameTemp];
}
-(void)setTotalHeight{
    totalHeight = 0.2;
    for(Message *menTemp in arrMensajes){
        [self setHeightWithMensaje:menTemp];
        totalHeight = totalHeight + [menTemp getFrame].size.height + 20.0 + cellPadding*2.0;
    }
}
-(void)cargarMensajes:(id)sender{
    if(loadMoreMessages){
        NSArray *arrMensajesTemp = [MessageController getConversationWithUser:self.userFriend withOffset:indPage withCount:15];
        if(arrMensajesTemp.count > 0){
            float previousTotalHeight = totalHeight;
            if(arrMensajesTemp.count < 15){
                loadMoreMessages = false;
            }
            int prevCant = (int)arrMensajes.count;
            [arrMensajes addObjectsFromArray:arrMensajesTemp];
            [self setTotalHeight];
            indPage++;
            if(prevCant == 0){
                self.messagesTableView.contentOffset = CGPointMake(0, totalHeight - self.messagesTableView.frame.size.height);
            }else{
                [self.messagesTableView reloadData];
                self.messagesTableView.contentOffset = CGPointMake(0, totalHeight - previousTotalHeight - 63);
            }
        }else{
            loadMoreMessages = false;
        }
    }
    [refCtrl endRefreshing];
}
-(void)disconnectToChannel{
    [fayeClient disconnect];
    self.sendBtn.enabled = false;
}
-(void)connectToChannel{
    [fayeClient connect];
}
-(IBAction)mostrarDetalleAmigo:(id)sender{
    if(!showingProfile){
        showingProfile = true;
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlGetProfileWithUserId:self.userFriend.userId] withParams:nil];
        [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *data, NSError *error){
            [SVProgressHUD dismiss];
            if(error == nil){
                NSError *jsonError = nil;
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError == nil){
                    NSDictionary *dicProfile = result[@"profile"];
                    NSDictionary *dicGallery = result[@"gallery"];
                    NSMutableDictionary *dicProfileFinal = [NSMutableDictionary dictionaryWithDictionary:dicProfile];
                    NSDictionary *dicPictures = dicGallery[@"gallery"];
                    NSMutableArray *arrPictures = [NSMutableArray arrayWithCapacity:0];
                    NSMutableArray *arrImages = [NSMutableArray arrayWithCapacity:0];
                    for(int i = 1; i < 7; i++){
                        NSString *strKey = [NSString stringWithFormat:@"picture_%i", i];
                        id strUrl = dicPictures[strKey][strKey][@"url"];
                        if(strUrl != [NSNull null]){
                            [arrPictures addObject:strUrl];
                            [arrImages addObject:[NSNull null]];
                        }else{
                            i = 7;
                        }
                    }
                    [dicProfileFinal setObject:arrImages forKey:@"images"];
                    [dicProfileFinal setObject:arrPictures forKey:@"pictures"];
                    [dicProfileFinal setObject:@"0" forKey:@"indActual"];
                    dicProfileToSend = dicProfileFinal;
                    [self performSegueWithIdentifier:@"showProfile" sender:self];
                }else{
                    [ToolBox showGenericErrorInViewCont:self];
                }
            }else{
                [ToolBox showConnectionErrorInViewCont:self];
            }
        }];
    }
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int indexSelectedTemp = (int)arrMensajes.count - indexSelectedCell;
    if(indexSelectedCell > -1){
        if((indexSelectedTemp) == indexPath.row){
            return 20;
        }
    }
    int ind = (int)indexPath.row;
    if(indexSelectedCell > -1){
        if(ind > indexSelectedTemp){
            ind--;
        }
    }
    ind = (int)arrMensajes.count - ind - 1;
    Message *menTemp = [arrMensajes objectAtIndex:ind];
    return [menTemp getFrame].size.height + 10.0*2.0 + cellPadding*2.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMensajes.count + ((indexSelectedCell > -1)?1:0);
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int indexSelectedTemp = (int)arrMensajes.count - indexSelectedCell;
    if(indexSelectedCell > -1){
        if((indexSelectedTemp) == indexPath.row){
            SentDateTableViewCell *dateCell = [tableView dequeueReusableCellWithIdentifier:@"dateCell" forIndexPath:indexPath];
            Message *menTemp = [arrMensajes objectAtIndex:indexSelectedCell];
            dateCell.date = menTemp.date;
            dateCell.side = ([menTemp.sender.userId isEqualToString:userMe.userId])?RightSide:LeftSide;
            return dateCell;
        }
    }
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];
    int ind = (int)indexPath.row;
    if(indexSelectedCell > -1){
        if(ind > indexSelectedTemp){
            ind--;
        }
    }
    ind = (int)arrMensajes.count - ind - 1;
    Message *menTemp = [arrMensajes objectAtIndex:ind];
    Message *menAnt = ((ind + 1) == arrMensajes.count)?nil:[arrMensajes objectAtIndex:(ind + 1)];
    Message *menSig = (ind == 0)?nil:[arrMensajes objectAtIndex:(ind - 1)];
    
    float widthTemp = [menTemp getFrame].size.width + 24.0;
    int posIniX = ([menTemp.sender.userId isEqualToString:userMe.userId])?(cell.frame.size.width - 8 - widthTemp):51;
    cell.friendImage = nil;
    if(![menTemp.sender.userId isEqualToString:userMe.userId]){
        BOOL mostrarFoto = NO;
        if(menSig != nil){
            if(![menTemp.sender.userId isEqualToString:menSig.sender.userId]){
                mostrarFoto = YES;
            }
        }else{
            mostrarFoto = YES;
        }
        if(mostrarFoto){
            cell.friendImage = [UIImage imageWithData:menTemp.sender.avatar];
            [cell.friendBtn addTarget:self action:@selector(mostrarDetalleAmigo:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    cell.side = ([userMe.userId isEqualToString:menTemp.sender.userId])?RightSide:LeftSide;
    cell.viewFrame = CGRectMake(posIniX, cellPadding, widthTemp, cell.frame.size.height - cellPadding*2.0);
    cell.textColor = ([menTemp.sender.userId isEqualToString:userMe.userId])?[UIColor whiteColor]:[UIColor blackColor];
    cell.topTypeCorner = NormalCorner;
    cell.bottomTypeCorner = NormalCorner;
    if(menAnt){
        if([menAnt.sender.userId isEqualToString:menTemp.sender.userId]){
            cell.topTypeCorner = TinyCorner;
        }
    }
    if(menSig){
        if([menSig.sender.userId isEqualToString:menTemp.sender.userId]){
            cell.bottomTypeCorner = TinyCorner;
        }
    }
    cell.strMessage = menTemp.message;
    [cell drawCell];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[MessageTableViewCell class]]){
        if(indexSelectedCell == -1){
            indexSelectedCell = (int)arrMensajes.count - (int)indexPath.row - 1;
            [tableView beginUpdates];
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
            if(indexPath.row + 1 == arrMensajes.count){
                if(tableView.contentSize.height + 20 > (tableView.frame.size.height - tableView.contentInset.bottom)){
                    [tableView setContentOffset:CGPointMake(0, tableView.contentSize.height - tableView.frame.size.height + tableView.contentInset.bottom + 20) animated:true];
                }
            }
        }else if ((arrMensajes.count - indexSelectedCell - 1) == indexPath.row){
            indexSelectedCell = -1;
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
        }else{
            int previousIndexSelectedCell = (int)arrMensajes.count - indexSelectedCell - 1;
            int indexSelectedTemp = (int)indexPath.row;
            if(indexSelectedTemp > previousIndexSelectedCell){
                indexSelectedTemp--;
            }
            indexSelectedCell = (int)arrMensajes.count - indexSelectedTemp - 1;
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(previousIndexSelectedCell + 1) inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(indexSelectedTemp + 1) inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
            if(indexPath.row == arrMensajes.count){
                if(tableView.contentSize.height > (tableView.frame.size.height - tableView.contentInset.bottom)){
                    [tableView setContentOffset:CGPointMake(0, tableView.contentSize.height - tableView.frame.size.height + tableView.contentInset.bottom) animated:true];
                }
            }
        }
    }
}
@end
