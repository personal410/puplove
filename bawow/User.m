//
//  User.m
//  bawow
//
//  Created by victor salazar on 16/05/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "User.h"
@implementation User
@dynamic avatar;
@dynamic email;
@dynamic fbUserId;
@dynamic firstName;
@dynamic lastName;
@dynamic me;
@dynamic userId;
@dynamic channel;
@dynamic token;
@dynamic profile;
@dynamic receivedMessages;
@dynamic sentMessages;
@dynamic lastMessage;
@dynamic searchDistance;
@dynamic matchOn;
@end