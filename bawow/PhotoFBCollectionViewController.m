//
//  PhotoCollectionViewController.m
//  bawow
//
//  Created by victor salazar on 29/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "PhotoFBCollectionViewController.h"
@implementation PhotoFBCollectionViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    selectedImage = NO;
    if(![self.albumId isEqualToString:@""]){
        [self getPhotosWithAccessToken:nil];
    }
}
-(void)getPhotosWithAccessToken:(NSString *)token{
    NSString *newToken = (token == nil)?@"":[NSString stringWithFormat:@"?%@", token];
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"/%@/photos%@", self.albumId, newToken] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *con, NSDictionary *result, NSError *error){
        if(error == nil){
            NSArray *data = [result objectForKey:@"data"];
            [self.arrPhotos addObjectsFromArray:data];
            NSDictionary *paging = [result objectForKey:@"paging"];
            NSString *strNextPath = [paging objectForKey:@"next"];
            if(strNextPath){
                NSArray *arrNextPath = [strNextPath componentsSeparatedByString:@"?"];
                [self getPhotosWithAccessToken:[arrNextPath lastObject]];
            }else{
                [self.collectionView reloadData];
            }
        }else{
            NSArray *data = [result objectForKey:@"data"];
            [self.arrPhotos addObjectsFromArray:data];
            NSDictionary *paging = [result objectForKey:@"paging"];
            NSString *strNextPath = [paging objectForKey:@"next"];
            if(strNextPath){
                NSArray *arrNextPath = [strNextPath componentsSeparatedByString:@"?"];
                [self getPhotosWithAccessToken:[arrNextPath lastObject]];
            }else{
                [self.collectionView reloadData];
            }
        }
    }];
}
#pragma mark <UICollectionViewDataSource>
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float totalWidth = self.collectionView.frame.size.width;
    float newTotalWidth = totalWidth - 20;
    float widthTemp = newTotalWidth/4.0;
    return CGSizeMake(widthTemp, widthTemp);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.arrPhotos.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoFBCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    cell.imgView.image = nil;
    NSDictionary *dicPhoto = [self.arrPhotos objectAtIndex:indexPath.row];
    NSString *strPicture = [dicPhoto objectForKey:@"picture"];
    NSURL *urlTemp = [NSURL URLWithString:strPicture];
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:urlTemp] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *d, NSError *e){
        cell.imgView.image = [UIImage imageWithData:d];
    }];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(!selectedImage){
        PhotoFBCollectionViewCell *cell = (PhotoFBCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.blackView.hidden = NO;
        [cell.loadActIndView startAnimating];
        NSDictionary *dicPhotoTemp = [self.arrPhotos objectAtIndex:indexPath.row];
        NSString *strSourceFirstImgTemp = [dicPhotoTemp objectForKey:@"source"];
        NSURL *urlSourceFirstImgTemp = [NSURL URLWithString:strSourceFirstImgTemp];
        NSURLRequest *req = [NSURLRequest requestWithURL:urlSourceFirstImgTemp];
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *d, NSError *e){
            imgTemp = [UIImage imageWithData:d];
            [self performSegueWithIdentifier:@"cropPhoto" sender:self];
            [cell.loadActIndView stopAnimating];
            cell.blackView.hidden = YES;
        }];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    CropPhotoViewController *destViewCont = [segue destinationViewController];
    destViewCont.navigationController.navigationBarHidden = YES;
    destViewCont.image = imgTemp;
}
@end