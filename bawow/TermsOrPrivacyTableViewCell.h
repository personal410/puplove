//
//  TermsOrPrivacyTableViewCell.h
//  bawow
//
//  Created by victor salazar on 1/08/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface TermsOrPrivacyTableViewCell:UITableViewCell
@property(nonatomic, weak) IBOutlet UIButton *termsOrPrivacyBtn;
@end