//
//  SettingsViewController.h
//  bawow
//
//  Created by victor salazar on 14/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UsuarioViewController.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CustomViewController.h"
@interface SettingsViewController:CustomViewController<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic,weak) IBOutlet UITableView *settingsTableView;
@end