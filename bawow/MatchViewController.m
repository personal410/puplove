//
//  MatchViewController.m
//  bawow
//
//  Created by victor salazar on 24/07/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "MatchViewController.h"
#import "UsuarioViewController.h"
@implementation MatchViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSSet *photos = [[[UserController getUser] profile] photos];
    for(Photo *photoTemp in photos){
        if([photoTemp.indPhoto isEqualToNumber:@1]){
            self.userImgView.image = [UIImage imageWithData:photoTemp.photo];
            break;
        }
    }
    self.friendImgView.image = [UIImage imageWithData:self.userFriend.avatar];
}
-(IBAction)actionDissmiss:(id)sender{
    UsuarioViewController *usuViewCont = (UsuarioViewController *)self.presentingViewController.parentViewController;
    usuViewCont.panEnabled = true;
    [self dismissViewControllerAnimated:true completion:nil];
}
@end