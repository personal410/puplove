//
//  User.h
//  bawow
//
//  Created by victor salazar on 16/05/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Message, Profile;
@interface User : NSManagedObject
@property (nonatomic, retain) NSData * avatar;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fbUserId;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * me;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * channel;
@property (nonatomic, retain) NSString * token;
@property (nonatomic, retain) NSString * lastMessage;
@property (nonatomic, retain) Profile *profile;
@property (nonatomic, retain) NSSet *receivedMessages;
@property (nonatomic, retain) NSSet *sentMessages;
@property (nonatomic, retain) NSNumber * searchDistance;
@property(nonatomic,retain) NSDate *matchOn;
@end