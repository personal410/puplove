//
//  UsuarioViewController.h
//  bawow
//
//  Created by victor salazar on 1/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "MenuViewController.h"
@interface UsuarioViewController:UIViewController<UIGestureRecognizerDelegate>{
    CGPoint puntoInicial;
    UINavigationController *playViewCont;
    BOOL showingMenu;
}
@property(nonatomic,weak) IBOutlet UIView *contentView;
@property(nonatomic,weak) IBOutlet UIImageView *backgroundImgView2;
@property(nonatomic) BOOL panEnabled;
-(void)cambiarViewCont:(int)ind;
-(void)showOrHideMenu;
-(void)showConfProfile;
@end