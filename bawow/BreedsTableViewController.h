//
//  BreedsTableViewController.h
//  bawow
//
//  Created by victor salazar on 6/05/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ResultSearchBreedsTableViewController.h"
@interface BreedsTableViewController:UITableViewController<UISearchResultsUpdating,UISearchControllerDelegate>{
    NSArray *arrBreeds;
    NSArray *searchResults;
    int indSeleccionado;
    int indSeleccionadoResult;
    UISearchController *searchCont;
    ResultSearchBreedsTableViewController *resultSearchBreedsTableViewCont;
}
@property(nonatomic,strong) NSString *currentBreed;
@property(nonatomic,weak) User *currentUser;
@end