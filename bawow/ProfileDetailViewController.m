//
//  ProfileDetailViewController.m
//  bawow
//
//  Created by victor salazar on 24/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "ProfileDetailViewController.h"
#import "UserController.h"
#import "User.h"
#import "ServerConnection.h"
@implementation ProfileDetailViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSLog(@"%@", self.dicProfile);
    if(self.shouldAnimated){
        transitionDelegate  = [[PlayTransitionDelegate alloc] init];
        self.transitioningDelegate = transitionDelegate;
    }
    self.nameBreedLbl.text = [NSString stringWithFormat:@"%@ (%@)", self.dicProfile[@"name"], self.dicProfile[@"breed"]];
    NSString *gender = [[self.dicProfile objectForKey:@"gender"] lowercaseString];
    int indGender = (int)[[[Diccionario sharedDiccionario] arrValoresOpcSegCtrlGender] indexOfObject:gender];
    gender = [[[[Diccionario sharedDiccionario] arrOpcionesSegCtrlConfPer] objectAtIndex:indGender] capitalizedString];
    NSString *strBirthday = [self.dicProfile objectForKey:@"birthday"];
    NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateBirthday = [dateFormatter dateFromString:strBirthday];
    NSDate *today = [NSDate date];
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dateBirthday toDate:today options:0];
    int cant = (dateComponents.year > 0)?((int)dateComponents.year):((int)dateComponents.month);
    int numero = (cant > 1)?1:0;
    NSString *yearOrMonthString = (dateComponents.year > 0)?([[Diccionario sharedDiccionario] strAnioConNumero:numero]):([[Diccionario sharedDiccionario] strMesConNumero:numero]);
    int pedigri = [[self.dicProfile objectForKey:@"pedigri"] intValue];
    NSString *strPedigri = [@[@"No", @"Yes"] objectAtIndex:pedigri];
    self.agePedigriLbl.text = [NSString stringWithFormat:@"- %@, %i %@\n- Pedigri: %@", gender, cant, yearOrMonthString, strPedigri];
    self.descriptionLbl.text = self.dicProfile[@"description"];
    int indActual = [self.dicProfile[@"indActual"] intValue];
    NSMutableArray *arrImages = self.dicProfile[@"images"];
    arrFotos = [NSMutableArray arrayWithCapacity:0];
    self.imagesPageCtrl.numberOfPages = arrImages.count;
    self.imagesPageCtrl.currentPage = indActual;
    self.imagesCollectionView.contentOffset = CGPointMake(self.imagesCollectionView.frame.size.width*indActual, 0);
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.imagesCollectionView.hidden = false;
}
-(IBAction)pageControlChangeValue:(UIPageControl *)sender{
    [self.imagesCollectionView setContentOffset:CGPointMake((int)sender.currentPage*self.imagesCollectionView.frame.size.width, 0) animated:YES];
}
-(IBAction)reportUser:(id)sender{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Report User" message:@"Do you wish to report inappropriate content from this user?" preferredStyle:UIAlertControllerStyleAlert];
    [alertCont addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [alertCont addAction:[UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action){
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        User *curentUser = [UserController getUser];
        NSDictionary *dicParams = @{@"reported_id": self.dicProfile[@"user_id"]};
        NSURLRequest *request = [ToolBox createRequestWithUrl:[URLs urlReportUserWithUserId:curentUser.userId] withParams:[ToolBox dictionaryToPostParams:dicParams]];
        [ServerConnection addRequest:request WithCompletitionHandler:^(NSData *data, NSError *error) {
            [SVProgressHUD dismiss];
            if(error){
                [ToolBox showConnectionErrorInViewCont:self];
            }else{
                NSError *jsonError = nil;
                id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError == nil){
                    if([result isKindOfClass:[NSNumber class]]){
                        int finalResult = ((NSNumber *)result).intValue;
                        if(finalResult == 1){
                            [ToolBox showAlertWith:@"Alert" and:@"The user has been reported successfully" inViewCont:self];
                            return;
                        }else if(finalResult == 2){
                            [ToolBox showAlertWith:@"Alert" and:@"The user has already been reported. It is being evaluated by our team." inViewCont:self];
                            return;
                        }
                    }
                }
                [ToolBox showAlertWith:@"Alert" and:@"The user could not be reported" inViewCont:self];
            }
        }];
    }]];
    [self presentViewController:alertCont animated:true completion:nil];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imagesPageCtrl.numberOfPages;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.imagesPageCtrl.currentPage = scrollView.contentOffset.x/scrollView.frame.size.width;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *imgViewTemp = (UIImageView *)[cell viewWithTag:1];
    NSMutableArray *arrImages = self.dicProfile[@"images"];
    int item = (int)indexPath.item;
    self.dicProfile[@"indActual"] = [NSNumber numberWithInt:item];
    if(arrImages[item] == [NSNull null]){
        NSArray *arrPictures = self.dicProfile[@"pictures"];
        NSString *strPicUrl = arrPictures[item];
        NSString *strUrlPicFinal = [NSString stringWithFormat:@"%@%@", [URLs urlIp], strPicUrl];
        NSURL *urlPic = [NSURL URLWithString:strUrlPicFinal];
        NSURLRequest *urlReq = [NSURLRequest requestWithURL:urlPic];
        [NSURLConnection sendAsynchronousRequest:urlReq queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *res, NSData *data, NSError *error){
            if(error == nil){
                imgViewTemp.image = [UIImage imageWithData:data];
                [arrImages replaceObjectAtIndex:item withObject:[UIImage imageWithData:data]];
            }else{
                NSLog(@"%s error: %@", __FUNCTION__, error);
                [ToolBox showConnectionErrorInViewCont:self];
            }
        }];
    }else{
        imgViewTemp.image = arrImages[item];
    }
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width);
}
-(IBAction)actionOK:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
