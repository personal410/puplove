//
//  MenuViewController.m
//  bawow
//
//  Created by victor salazar on 5/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "MenuViewController.h"
#import "Diccionario.h"
@implementation MenuViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.profileImgView.layer.cornerRadius = 40;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self actualizarFotoPerfil];
}
-(IBAction)mostrarPerfil{
    UsuarioViewController *viewCont = (UsuarioViewController *)self.parentViewController;
    [viewCont showConfProfile];
}
-(void)actualizarFotoPerfil{
    NSSet *photos = [[[UserController getUser] profile] photos];
    for(Photo *photoTemp in photos){
        if([photoTemp.indPhoto isEqualToNumber:@1]){
            self.profileImgView.image = [UIImage imageWithData:photoTemp.photo];
            break;
        }
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"opcionCell" forIndexPath:indexPath];
    UILabel *lblTemp = (UILabel *)[cell viewWithTag:1];
    lblTemp.text = [[[Diccionario sharedDiccionario] arrOpcionesMenu] objectAtIndex:indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UsuarioViewController *viewCont = (UsuarioViewController *)self.parentViewController;
    [viewCont cambiarViewCont:(int)indexPath.row];
}
@end
