//
//  ProfileDetailViewController.h
//  bawow
//
//  Created by victor salazar on 24/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "PlayTransitionDelegate.h"
@class PlayTransitionDelegate;
@interface ProfileDetailViewController:UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>{
    int cantFotos;
    NSMutableArray *arrFotos;
    PlayTransitionDelegate *transitionDelegate;
}
@property(nonatomic) BOOL shouldAnimated;
@property(nonatomic,weak) NSMutableDictionary *dicProfile;
@property(nonatomic,weak) IBOutlet UIPageControl *imagesPageCtrl;
@property(nonatomic,weak) IBOutlet UICollectionView *imagesCollectionView;
@property(nonatomic,weak) IBOutlet UIView *topView;
@property(nonatomic,weak) IBOutlet UIView *infoContainerView;
@property(nonatomic,weak) IBOutlet UILabel *nameBreedLbl;
@property(nonatomic,weak) IBOutlet UILabel *agePedigriLbl;
@property(nonatomic,weak) IBOutlet UILabel *descriptionLbl;
@property(nonatomic,weak) IBOutlet UIImageView *imgViewTemp;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *descBottomSpaceCons;
@end