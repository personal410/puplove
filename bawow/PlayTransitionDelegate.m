//
//  PlayTransitionDelegate.m
//  bawow
//
//  Created by victor salazar on 23/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "PlayTransitionDelegate.h"
@implementation PlayTransitionDelegate
-(instancetype)init{
    if(self = [super init]){
        isPresenting = false;
    }
    return self;
}
-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    NSLog(@"%i", isPresenting);
    UIView *view = [transitionContext containerView];
    view.backgroundColor = [UIColor whiteColor];
    id fromViewCont = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    id toViewCont = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UsuarioViewController *usuarioViewCont = (isPresenting)?(fromViewCont):(toViewCont);
    PlayViewController *playViewCont = nil;
    for(id viewCont in [usuarioViewCont childViewControllers]){
        if([viewCont isKindOfClass:[UINavigationController class]]){
            playViewCont = (PlayViewController *)[viewCont topViewController];
            break;
        }
    }
    ProfileDetailViewController *profileViewCont = (isPresenting)?(toViewCont):(fromViewCont);
    if(isPresenting){
        [view addSubview:profileViewCont.view];
    }else{
        [view insertSubview:usuarioViewCont.view aboveSubview:profileViewCont.view];
    }
    CGAffineTransform infoContainerViewTranslation = CGAffineTransformMakeTranslation(0, profileViewCont.infoContainerView.frame.size.height);
    CGAffineTransform topViewTranslation = CGAffineTransformMakeTranslation(0, -profileViewCont.topView.frame.size.height);
    if(isPresenting){
        profileViewCont.imagesCollectionView.hidden = true;
        firstScale = playViewCont.profileImgView.frame.size.width/profileViewCont.view.frame.size.width;
    }else{
        profileViewCont.descBottomSpaceCons.constant = 200;
        profileViewCont.view.backgroundColor = [UIColor clearColor];
        int intActual = [profileViewCont.dicProfile[@"indActual"] intValue];
        NSArray *images = [profileViewCont.dicProfile objectForKey:@"images"];
        id imgActual = images[intActual];
        if(imgActual != [NSNull null]){
            playViewCont.profileImgView.image = imgActual;
        }
    }
    CGAffineTransform imgViewScale = CGAffineTransformMakeScale(firstScale, firstScale);
    if(isPresenting){
        profileViewCont.infoContainerView.transform = infoContainerViewTranslation;
        profileViewCont.topView.transform = topViewTranslation;
        profileViewCont.imgViewTemp.image = playViewCont.profileImgView.image;
        profileViewCont.imgViewTemp.transform = imgViewScale;
    }
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        if(isPresenting){
            profileViewCont.infoContainerView.transform = CGAffineTransformIdentity;
            profileViewCont.topView.transform = CGAffineTransformIdentity;
            profileViewCont.imgViewTemp.transform = CGAffineTransformIdentity;
        }else{
            usuarioViewCont.view.transform = CGAffineTransformIdentity;
            profileViewCont.infoContainerView.transform = infoContainerViewTranslation;
            profileViewCont.topView.transform = topViewTranslation;
            profileViewCont.imagesCollectionView.transform = imgViewScale;
        }
    } completion:^(BOOL finished){
        profileViewCont.imgViewTemp.hidden = true;
        profileViewCont.imagesCollectionView.hidden = false;
        [[[UIApplication sharedApplication] keyWindow] addSubview:[toViewCont view]];
        if(isPresenting){
            profileViewCont.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.4];
            profileViewCont.descBottomSpaceCons.constant = 8;
        }
        [transitionContext completeTransition:YES];
    }];
}
-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return 0.5;
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    isPresenting = true;
    return self;
}
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    isPresenting = false;
    return self;
}
@end