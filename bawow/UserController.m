//
//  UserController.m
//  bawow
//
//  Created by victor salazar on 17/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "UserController.h"
#import "Message.h"
@implementation UserController
+(User *)getUser{
    NSArray *users = [self getUsersWithMe:true];
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    if(users.count == 0){
        return nil;
    }else{
        User *sharedUser = [users firstObject];
        if(!sharedUser.profile){
            sharedUser.profile = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
        }
        return sharedUser;
    }
}
+(NSArray *)getUsersWithMe:(BOOL)me{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSFetchRequest *fetchReq = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [fetchReq setPredicate:[NSPredicate predicateWithFormat:@"me == %@", [NSNumber numberWithBool:me]]];
    NSError *error = nil;
    NSArray *lista = [context executeFetchRequest:fetchReq error:&error];
    return lista;
}
+(NSArray *)getFriends{
    return [self getUsersWithMe:false];
}
+(void)deleteUser{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    User *userMe = [self getUser];
    [context deleteObject:userMe];
    NSArray *arrUserFriends = [self getFriends];
    for(User *userFriend in arrUserFriends){
        [context deleteObject:userFriend];
    }
    NSError *error = nil;
    [context save:&error];
    if (error) {
        NSLog(@"error %s: %@", __FUNCTION__, error);
    }
}
+(NSArray *)getAllUsers{
    NSFetchRequest *fetchReq = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSError *error = nil;
    NSArray *lista = [context executeFetchRequest:fetchReq error:&error];
    if(error){
        NSLog(@"error %s: %@", __FUNCTION__, error.description);
    }
    return lista;
}
+(User *)getUserWithUserId:(NSString *)strUserId{
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSFetchRequest *fetchReq = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [fetchReq setPredicate:[NSPredicate predicateWithFormat:@"userId == %@", strUserId]];
    NSError *error = nil;
    NSArray *lista = [context executeFetchRequest:fetchReq error:&error];
    return lista.firstObject;
}
+(void)updateUser:(User *)user withDictionary:(NSDictionary *)dic{
    for(NSString *key in dic.allKeys){
        [user setValue:[dic objectForKey:key] forKey:key];
    }
    NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSError *error;
    if(![context save:&error]){
        NSLog(@"%@", error);
    }
}
@end