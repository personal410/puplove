//
//  PlayViewController.m
//  bawow
//
//  Created by victor salazar on 8/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "PlayViewController.h"
#import "MatchViewController.h"
@implementation PlayViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    shouldLoadMore = true;
    requestingMoreProfiles = false;
    didChangeSearchDistance = false;
    arrProfiles = [NSMutableArray arrayWithCapacity:0];
    self.containerImgView.layer.cornerRadius = 15.0f;
    self.containerImgView.layer.borderWidth = 1.0;
    self.containerImgView.layer.borderColor = [[UIColor colorWithRed:0.7569 green:0.7569 blue:0.7569 alpha:1.0] CGColor];
    [self startAnimationLoadingView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isActive = true;
    if(!didChangeSearchDistance){
        didChangeSearchDistance = [[Diccionario sharedDiccionario] didChangeSearchDistance];
        [[Diccionario sharedDiccionario] setDidChangeSearchDistance:false];
    }
    BOOL loadMore = false;
    if(arrProfiles.count == 0 && !requestingMoreProfiles){
        if(didChangeSearchDistance){
            loadMore = true;
        }else{
            loadMore = shouldLoadMore;
        }
    }
    if(loadMore){
        if(locManager == nil){
            locManager = [[CLLocationManager alloc] init];
            locManager.delegate = self;
            locManager.distanceFilter = kCLDistanceFilterNone;
        }
        if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
            self.loadingView.hidden = false;
            self.loadingLbl.text = @"Finding dogs near you...";
            [self startAnimationLoadingView];
            [locManager startUpdatingLocation];
            requestingMoreProfiles = true;
            didObtainLocation = false;
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    isActive = false;
}
#pragma mark - CLLocationManager Methods
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    [manager stopUpdatingLocation];
    if(!didObtainLocation){
        didObtainLocation = true;
        CLLocation *lastLocation = locations.lastObject;
        CLLocationCoordinate2D coordinate = lastLocation.coordinate;
        User *user = [UserController getUser];
        NSDictionary *dicParams = @{@"latitude": [NSNumber numberWithFloat:coordinate.latitude], @"longitude": [NSNumber numberWithFloat:coordinate.longitude], @"distance": user.searchDistance};
        NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlGetProfilesWithUserId:user.userId] withParams:[ToolBox dictionaryToPostParams:dicParams]];
        [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *dataGetProfiles, NSError *error){
            [self stopAnimationLoadingView];
            requestingMoreProfiles = false;
            didChangeSearchDistance = false;
            [[Diccionario sharedDiccionario] setDidChangeSearchDistance:false];
            if(error == nil){
                NSError *jsonError = nil;
                id resultGetProfiles = [NSJSONSerialization JSONObjectWithData:dataGetProfiles options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError == nil){
                    [self convertirResultado:resultGetProfiles];
                    [self loadProfile];
                }else{
                    if(isActive){
                        [ToolBox showGenericErrorInViewCont:self];
                    }
                }
            }else{
                if(isActive){
                    [ToolBox showConnectionErrorInViewCont:self];
                }
            }
        }];
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status == kCLAuthorizationStatusNotDetermined){
        [locManager requestWhenInUseAuthorization];
    }else if(status == kCLAuthorizationStatusAuthorizedWhenInUse){
        BOOL loadMore = false;
        if(arrProfiles.count == 0 && !requestingMoreProfiles){
            if(didChangeSearchDistance){
                loadMore = true;
            }else{
                loadMore = shouldLoadMore;
            }
        }
        if(loadMore){
            self.loadingLbl.text = @"Finding dogs near you...";
            [self startAnimationLoadingView];
            [locManager startUpdatingLocation];
            requestingMoreProfiles = true;
            didObtainLocation = false;
        }
    }else{
        [self stopAnimationLoadingView];
        self.loadingView.hidden = false;
        self.loadingLbl.text = @"Please turn on your location services to be able to use this feature.";
    }
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self stopAnimationLoadingView];
    self.loadingView.hidden = false;
    if(error.code == 1){
        self.loadingLbl.text = @"Please turn on your location services to be able to use this feature.";
    }else{
        self.loadingLbl.text = @"An unexpected error occurred.";
        self.loadingLbl.text = @"Couldnt get your location.";
    }
}
#pragma mark - Animation Methods
-(void)startAnimationLoadingView{
    if([self.loadingImageView.layer animationForKey:@"rotation"] == nil){
        CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        fullRotation.fromValue = @0;
        fullRotation.toValue = [NSNumber numberWithFloat:2.0*M_PI];
        fullRotation.duration = 2.0;
        fullRotation.speed = 1;
        fullRotation.repeatCount = MAXFLOAT;
        self.loadingImageView.hidden = false;
        [self.loadingImageView.layer addAnimation:fullRotation forKey:@"rotation"];
    }
}
-(void)stopAnimationLoadingView{
    self.loadingImageView.hidden = true;
    [self.loadingImageView.layer removeAnimationForKey:@"rotation"];
}
#pragma mark - Other Methods
-(void)convertirResultado:(NSArray *)lista{
    if(lista.count == 20){
        shouldLoadMore = true;
    }else{
        shouldLoadMore = false;
    }
    for(NSArray *arrProfWithPic in lista){
        NSDictionary *dicProfile = arrProfWithPic[0][@"profile"];
        NSDictionary *dicGallery = arrProfWithPic[1];
        NSMutableDictionary *dicProfileFinal = [NSMutableDictionary dictionaryWithDictionary:dicProfile];
        NSDictionary *dicPictures = dicGallery[@"gallery"];
        NSMutableArray *arrPictures = [NSMutableArray arrayWithCapacity:0];
        NSMutableArray *arrImages = [NSMutableArray arrayWithCapacity:0];
        for(int i = 1; i < 7; i++){
            NSString *strKey = [NSString stringWithFormat:@"picture_%i", i];
            id strUrl = dicPictures[strKey][strKey][@"url"];
            if(strUrl != [NSNull null]){
                [arrPictures addObject:strUrl];
                [arrImages addObject:[NSNull null]];
            }else{
                i = 7;
            }
        }
        [dicProfileFinal setObject:arrImages forKey:@"images"];
        [dicProfileFinal setObject:arrPictures forKey:@"pictures"];
        [dicProfileFinal setObject:@"0" forKey:@"indActual"];
        [arrProfiles addObject:dicProfileFinal];
    }
}
-(void)loadProfile{
    if(arrProfiles.count > 0){
        self.loadingView.hidden = true;
        self.containerImgView.hidden = false;
        self.yesBtn.hidden = false;
        self.noBtn.hidden = false;
        self.profileImgView.image = nil;
        NSMutableDictionary *dicProfileTemp = arrProfiles.firstObject;
        self.profileNamelbl.text = [NSString stringWithFormat:@"%@ (%@)", dicProfileTemp[@"name"], dicProfileTemp[@"breed"]];
        NSMutableArray *arrImages = dicProfileTemp[@"images"];
        int indActual = [dicProfileTemp[@"indActual"] intValue];
        if([arrImages objectAtIndex:indActual] != [NSNull null]){
            self.profileImgView.image = [arrImages objectAtIndex:indActual];
        }else{
            NSArray *pictures = dicProfileTemp[@"pictures"];
            NSString *strUrl = pictures[indActual];
            NSString *strUrlPicFinal = [NSString stringWithFormat:@"%@%@", [URLs urlIp], strUrl];
            NSURL *urlPic1 = [NSURL URLWithString:strUrlPicFinal];
            NSURLRequest *urlReqPic = [NSURLRequest requestWithURL:urlPic1];
            [NSURLConnection sendAsynchronousRequest:urlReqPic queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                if(error == nil){
                    self.profileImgView.image = [UIImage imageWithData:data];
                    NSMutableArray *arrImages = dicProfileTemp[@"images"];
                    [arrImages replaceObjectAtIndex:0 withObject:[UIImage imageWithData:data]];
                }else{
                    NSLog(@"%s error: %@", __FUNCTION__, error);
                    if(isActive){
                        [ToolBox showConnectionErrorInViewCont:self];
                    }
                }
            }];
        }
    }else{
        self.containerImgView.hidden = true;
        self.yesBtn.hidden = true;
        self.noBtn.hidden = true;
        self.profileImgView.image = nil;
        self.profileNamelbl.text = @"";
        self.loadingView.hidden = false;
        self.loadingImageView.hidden = true;
        self.loadingLbl.text = @"There are no dogs near to you :(";
        BOOL loadMore = didChangeSearchDistance;
        if(!loadMore){
            loadMore = shouldLoadMore;
        }
        if(loadMore){
            if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
                if(locManager == nil){
                    locManager = [[CLLocationManager alloc] init];
                    locManager.delegate = self;
                    locManager.distanceFilter = kCLDistanceFilterNone;
                }
                self.loadingLbl.text = @"Finding dogs near you...";
                [self startAnimationLoadingView];
                [locManager startUpdatingLocation];
                requestingMoreProfiles = true;
                didObtainLocation = false;
            }
        }else{
            shouldLoadMore = true;
            [self performSelector:@selector(loadProfile) withObject:nil afterDelay:30];
        }
    }
}
#pragma mark - IBAction Methods
-(IBAction)actionYesOrNo:(UIButton *)sender{
    int ind = (int)sender.tag;
    if(tempView == nil){
        [self createTempView];
    }
    UIImageView *imgViewYes = (UIImageView *)[tempView viewWithTag:3];
    UIImageView *imgViewNo = (UIImageView *)[tempView viewWithTag:4];
    [UIView animateWithDuration:0.3 animations:^{
        if(ind == 0){
            imgViewNo.alpha = 1.0;
        }else{
            imgViewYes.alpha = 1.0;
        }
    } completion:^(BOOL finished){
        [self acceptOrDeclineProfileWithDecision:ind withAnimationDuration:0.5];
    }];
}
-(IBAction)showProfile:(id)sender{
    [self performSegueWithIdentifier:@"showProfile" sender:self];
}
-(IBAction)panProfile:(UIPanGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateBegan){
        initialPoint = [gesture locationInView:self.view];
        [self createTempView];
    }else if(gesture.state == UIGestureRecognizerStateChanged){
        CGPoint pointTemp = [gesture locationInView:self.view];
        float deltaX = pointTemp.x - initialPoint.x;
        float deltaY = pointTemp.y - initialPoint.y;
        CGAffineTransform translation = CGAffineTransformMakeTranslation(deltaX, deltaY);
        float semiWidth = self.containerImgView.frame.size.width/2.0;
        float semiHeight = self.containerImgView.frame.size.height/2.0;
        float deltaScaleX = 0.05*(semiWidth - MIN(semiWidth, fabsf(deltaX)))/semiWidth;
        float deltaScaleY = 0.05*(semiHeight - MIN(semiHeight, fabsf(deltaY)))/semiHeight;
        float minDeltaScale = MIN(deltaScaleX, deltaScaleY);
        float newScale = 1 - minDeltaScale;
        self.containerImgView.transform = CGAffineTransformMakeScale(newScale, newScale);
        float portionX = MIN(semiWidth, fabsf(deltaX))/semiWidth;
        float signo = (deltaX > 0) ? -1 : 1;
        float portion = signo*portionX;
        float tenGrades = M_PI/18.0;
        CGAffineTransform rotation = CGAffineTransformMakeRotation(tenGrades*portion);
        tempView.transform = CGAffineTransformConcat(translation, rotation);
        UIImageView *imgViewYes = (UIImageView *)[tempView viewWithTag:3];
        UIImageView *imgViewNo = (UIImageView *)[tempView viewWithTag:4];
        float quarterWidth = semiWidth*2.0/3.0;
        float portionX2 = MIN(quarterWidth, fabsf(deltaX))/quarterWidth;
        if(deltaX < 0){
            imgViewYes.alpha = 0.0;
            imgViewNo.alpha = portionX2;
        }else{
            imgViewNo.alpha = 0.0;
            imgViewYes.alpha = portionX2;
        }
    }else if(gesture.state == UIGestureRecognizerStateEnded){
        UIImageView *imgViewYes = (UIImageView *)[tempView viewWithTag:3];
        UIImageView *imgViewNo = (UIImageView *)[tempView viewWithTag:4];
        if(imgViewYes.alpha == 1.0 || imgViewNo.alpha == 1.0){
            int ind = imgViewYes.alpha == 1.0 ? 1 : 0;
            [self acceptOrDeclineProfileWithDecision:ind withAnimationDuration:0.3];
        }else{
            [UIView animateWithDuration:0.5 animations:^{
                tempView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                [tempView removeFromSuperview];
                UILabel *lblTemp = (UILabel *)[tempView viewWithTag:2];
                UIImageView *imgViewTemp = (UIImageView *)[tempView viewWithTag:1];
                self.containerImgView.transform = CGAffineTransformIdentity;
                self.profileImgView.image = imgViewTemp.image;
                self.profileNamelbl.text = lblTemp.text;
                self.containerImgView.hidden = false;
                tempView = nil;
            }];
        }
    }
}
-(void)createTempView{
    tempView = [[UIView alloc] initWithFrame:self.containerImgView.frame];
    tempView.backgroundColor = [UIColor whiteColor];
    tempView.layer.borderColor = self.containerImgView.layer.borderColor;
    tempView.layer.borderWidth = 1.0;
    tempView.layer.cornerRadius = self.containerImgView.layer.cornerRadius;
    UIImageView *imgViewTemp = [[UIImageView alloc] initWithImage:self.profileImgView.image];
    imgViewTemp.frame = self.profileImgView.frame;
    imgViewTemp.tag = 1;
    [tempView addSubview:imgViewTemp];
    UILabel *lblTemp = [[UILabel alloc] initWithFrame:self.profileNamelbl.frame];
    lblTemp.text = self.profileNamelbl.text;
    lblTemp.tag = 2;
    lblTemp.textAlignment = NSTextAlignmentCenter;
    [tempView addSubview:lblTemp];
    float spaceBetweenContainerAndImgView = (tempView.frame.size.width - imgViewTemp.frame.size.width);
    float imgSize = (tempView.frame.size.width - 2.0*spaceBetweenContainerAndImgView)/2.0;
    UIImageView *imgViewYes = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yes-button.png"]];
    imgViewYes.frame = CGRectMake(spaceBetweenContainerAndImgView/2.0, spaceBetweenContainerAndImgView/2.0, imgSize, imgSize);
    imgViewYes.tag = 3;
    imgViewYes.alpha = 0.0;
    [tempView addSubview:imgViewYes];
    UIImageView *imgViewNo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no-button.png"]];
    imgViewNo.frame = CGRectMake(tempView.frame.size.width - imgSize - spaceBetweenContainerAndImgView/2.0, spaceBetweenContainerAndImgView/2.0, imgSize, imgSize);
    imgViewNo.tag = 4;
    imgViewNo.alpha = 0.0;
    [tempView addSubview:imgViewNo];
    [self.view addSubview:tempView];
    if(arrProfiles.count == 1){
        self.containerImgView.hidden = true;
    }else{
        self.containerImgView.transform = CGAffineTransformMakeScale(0.95, 0.95);
        NSMutableDictionary *dicProfileTemp = [arrProfiles objectAtIndex:1];
        self.profileNamelbl.text = [NSString stringWithFormat:@"%@ (%@)", dicProfileTemp[@"name"], dicProfileTemp[@"breed"]];
        self.profileImgView.image = nil;
        NSMutableArray *arrImages = [dicProfileTemp objectForKey:@"images"];
        if(arrImages.firstObject != [NSNull null]){
            self.profileImgView.image = arrImages.firstObject;
        }else{
            NSArray *arrPictures = [dicProfileTemp objectForKey:@"pictures"];
            NSString *strUrl = arrPictures.firstObject;
            NSString *strUrlPicFinal = [NSString stringWithFormat:@"%@%@", [URLs urlIp], strUrl];
            NSURL *urlPic1 = [NSURL URLWithString:strUrlPicFinal];
            NSURLRequest *urlReqPic = [NSURLRequest requestWithURL:urlPic1];
            [NSURLConnection sendAsynchronousRequest:urlReqPic queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                if(error == nil){
                    self.profileImgView.image = [UIImage imageWithData:data];
                    [arrImages replaceObjectAtIndex:0 withObject:[UIImage imageWithData:data]];
                }else{
                    NSLog(@"%s error: %@", __FUNCTION__, error);
                    if(isActive){
                        [ToolBox showConnectionErrorInViewCont:self];
                    }
                }
            }];
        }
    }
}
-(void)acceptOrDeclineProfileWithDecision:(int)decision withAnimationDuration:(float)duration{
    int signo = decision*2 - 1;
    float widthTenGrades = tempView.frame.size.width*cosf(M_PI/18.0) + tempView.frame.size.height*sin(M_PI/18.0);
    float totalTranlation = (widthTenGrades + self.view.frame.size.width)/2.0;
    CGAffineTransform translationTrans = CGAffineTransformMakeTranslation(totalTranlation*(float)signo, 0);
    CGAffineTransform rotationTrans = CGAffineTransformMakeRotation((float)signo*M_PI/-18.0);
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        tempView.transform = CGAffineTransformConcat(rotationTrans, translationTrans);
        self.containerImgView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished){
        tempView = nil;
        BOOL result = [[NSNumber numberWithInteger:decision] boolValue];
        NSDictionary *dicProfile = arrProfiles.firstObject;
        NSString *strUserId = [dicProfile objectForKey:@"user_id"];
        NSString *strUrl = result?[URLs urlSetLikeWithUserId:[[UserController getUser] userId]]:[URLs urlSetDislikeWithUserId:[[UserController getUser] userId]];
        NSDictionary *dicParams = result?@{@"liked_id": strUserId}:@{@"disliked_id": strUserId};
        NSURLRequest *req = [ToolBox createRequestWithUrl:strUrl withParams:[ToolBox dictionaryToPostParams:dicParams]];
        [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *dataLike, NSError *error){
            if(error == nil){
                NSError *jsonError = nil;
                id dicResult = [NSJSONSerialization JSONObjectWithData:dataLike options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError == nil){
                    if(![dicResult isEqual:[NSNull null]]){
                        NSDictionary *dicUser = [dicResult objectForKey:@"user"];
                        NSString *strUrlPic1 = [dicUser objectForKey:@"profile_avatar"];
                        NSURL *urlPic1 = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [URLs urlIp], strUrlPic1]];
                        NSManagedObjectContext *context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
                        User *userTemp = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
                        userTemp.userId = [dicUser objectForKey:@"id"];
                        userTemp.email = [dicUser objectForKey:@"email"];
                        userTemp.fbUserId = [dicUser objectForKey:@"fb_user_id"];
                        userTemp.firstName = [dicUser objectForKey:@"first_name"];
                        userTemp.lastName = [dicUser objectForKey:@"last_name"];
                        userTemp.me = [NSNumber numberWithBool:false];
                        userTemp.lastMessage = @"";
                        userTemp.avatar = [NSData dataWithContentsOfURL:urlPic1];
                        userTemp.matchOn = [NSDate date];
                        Profile *profileTemp = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
                        profileTemp.profileId = [dicProfile objectForKey:@"id"];
                        userTemp.profile = profileTemp;
                        NSDictionary *dicConversation = dicUser[@"conversation"];
                        userTemp.channel = dicConversation[@"channel"];
                        userTemp.token = dicConversation[@"token"];
                        NSError *error2;
                        if(![context save:&error2]){
                            if(isActive){
                                [ToolBox showGenericErrorInViewCont:self];
                            }
                        }else{
                            userJustMatched = userTemp;
                            [self performSegueWithIdentifier:@"mostrarMatch" sender:self];
                        }
                    }
                }else{
                    if(isActive){
                        [ToolBox showGenericErrorInViewCont:self];
                    }
                }
            }else{
                NSLog(@"%s error: %@", __FUNCTION__, error);
                if(isActive){
                    [ToolBox showConnectionErrorInViewCont:self];
                }
            }
        }];
        [arrProfiles removeObjectAtIndex:0];
        [self loadProfile];
    }];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if(![segue.identifier isEqualToString:@"mostrarMatch"]){
        ProfileDetailViewController *viewCont = [segue destinationViewController];
        viewCont.shouldAnimated = true;
        viewCont.dicProfile = arrProfiles.firstObject;
    }else{
        UsuarioViewController *usuView = (UsuarioViewController *)self.navigationController.parentViewController;
        usuView.panEnabled = false;
        MatchViewController *matchView = segue.destinationViewController;
        matchView.userFriend = userJustMatched;
    }
}
@end
