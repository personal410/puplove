//
//  UserController.h
//  bawow
//
//  Created by victor salazar on 17/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "User.h"
#import "Profile.h"
#import "Photo.h"
@interface UserController:NSObject
+(User *)getUser;
+(NSArray *)getFriends;
+(void)deleteUser;
+(NSArray *)getAllUsers;
+(User *)getUserWithUserId:(NSString *)strUserId;
+(void)updateUser:(User *)user withDictionary:(NSDictionary *)dic;
@end