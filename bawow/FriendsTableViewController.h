//
//  MessagesTableViewController.h
//  bawow
//
//  Created by victor salazar on 13/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "FriendTableViewCell.h"
#import "UsuarioViewController.h"
#import "ChatViewController.h"
#import "CustomViewController.h"
@interface FriendsTableViewController:UITableViewController{
    NSArray *arrFriends;
}
@end