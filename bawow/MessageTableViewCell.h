//
//  MessageTableViewCell.h
//  bawow
//
//  Created by victor salazar on 3/04/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#define SizeNormalCorner 12.0f
#define SizeTinyCorner 4.0f
typedef NS_ENUM(NSInteger, Side){
    LeftSide,
    RightSide
};
typedef NS_ENUM(NSInteger, TypeCorner){
    NormalCorner,
    TinyCorner
};
@interface MessageTableViewCell:UITableViewCell{
    UIView *roundedView;
    UILabel *messageLbl;
    UIImageView *friendImgView;
}
@property(nonatomic) CGRect viewFrame;
@property(nonatomic,strong) UIImage *friendImage;
@property(nonatomic,strong) NSString *strMessage;
@property(nonatomic,strong) UIColor *textColor;
@property(nonatomic) Side side;
@property(nonatomic) TypeCorner topTypeCorner;
@property(nonatomic) TypeCorner bottomTypeCorner;
@property(nonatomic,strong) UIButton *friendBtn;
-(void)drawCell;
@end