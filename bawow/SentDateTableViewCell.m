//
//  SentDateTableViewCell.m
//  bawow
//
//  Created by victor salazar on 4/04/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "SentDateTableViewCell.h"
@implementation SentDateTableViewCell
-(void)setDate:(NSDate *)date{
    _date = date;
    NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
    self.sentDateLbl.text = [dateFormatter stringFromDate:self.date];
}
-(void)setSide:(Side)side{
    _side = side;
    self.sentDateLbl.textAlignment = (self.side == LeftSide)?NSTextAlignmentLeft:NSTextAlignmentRight;
}
@end