//
//  URLs.m
//  bawow
//
//  Created by victor salazar on 9/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "URLs.h"
@implementation URLs
+(NSString *)urlIp{
    return @"http://core.puplove.club";
}
+(NSString *)urlRaiz{
    return [NSString stringWithFormat:@"%@/api/v1/", [self urlIp]];
}
+(NSString *)urlUserWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@user/%@", [self urlRaiz], userId];
}
+(NSString *)urlSignIn{
    return [NSString stringWithFormat:@"%@sign_in", [self urlUserWithUserId:@""]];
}
+(NSString *)urlCreateProfileWithUserId:(NSString *)userId withProfileId:(NSString *)profileId{
    return [NSString stringWithFormat:@"%@/profile/%@", [self urlUserWithUserId:userId], profileId];
}
+(NSString *)urlGetProfilesWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/get_profiles", [self urlUserWithUserId:userId]];
}
+(NSString *)urlSyncUserWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/sync_user", [self urlUserWithUserId:userId]];
}
+(NSString *)urlSetLikeWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/set_like", [self urlUserWithUserId:userId]];
}
+(NSString *)urlSetDislikeWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/set_dislike", [self urlUserWithUserId:userId]];
}
+(NSString *)urlRetrieveMessagesWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/retrieve_messages", [self urlUserWithUserId:userId]];
}
+(NSString *)urlAllowPushWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/allow_push", [self urlUserWithUserId:userId]];
}
+(NSString *)urlGetProfileWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/get_profile", [self urlUserWithUserId:userId]];
}
+(NSString *)urlReportUserWithUserId:(NSString *)userId{
    return [NSString stringWithFormat:@"%@/report", [self urlUserWithUserId:userId]];
}
+(NSString *)urlTermsAgreement{
    return @"http://puplove.club/#/terms";
}
+(NSString *)urlPrivacyPolicy{
    return @"http://puplove.club/#/privacy";
}
@end
