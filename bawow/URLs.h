//
//  URLs.h
//  bawow
//
//  Created by victor salazar on 9/02/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface URLs:NSObject
+(NSString *)urlIp;
+(NSString *)urlSignIn;
+(NSString *)urlUserWithUserId:(NSString *)userId;
+(NSString *)urlCreateProfileWithUserId:(NSString *)userId withProfileId:(NSString *)profileId;
+(NSString *)urlGetProfilesWithUserId:(NSString *)userId;
+(NSString *)urlSyncUserWithUserId:(NSString *)userId;
+(NSString *)urlSetLikeWithUserId:(NSString *)userId;
+(NSString *)urlSetDislikeWithUserId:(NSString *)userId;
+(NSString *)urlRetrieveMessagesWithUserId:(NSString *)userId;
+(NSString *)urlAllowPushWithUserId:(NSString *)userId;
+(NSString *)urlGetProfileWithUserId:(NSString *)userId;
+(NSString *)urlReportUserWithUserId:(NSString *)userId;
+(NSString *)urlTermsAgreement;
+(NSString *)urlPrivacyPolicy;
@end