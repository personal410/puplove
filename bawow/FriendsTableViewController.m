//
//  MessagesTableViewController.m
//  bawow
//
//  Created by victor salazar on 13/01/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "FriendsTableViewController.h"
@implementation FriendsTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrFriends = [UserController getFriends];
    UIImage *img = [[UIImage imageNamed:@"Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:img style:UIBarButtonItemStyleDone target:self action:@selector(showOrHideMenu)];
    self.navigationItem.leftBarButtonItem = barButton;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.toolbar.translucent = YES;
    self.navigationController.toolbar.hidden = YES;
    [self.tableView reloadData];
}
-(void)showOrHideMenu{
    UsuarioViewController *usuViewContTemp = (UsuarioViewController *)(self.parentViewController.parentViewController);
    [usuViewContTemp showOrHideMenu];
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrFriends.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FriendTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"userCell" forIndexPath:indexPath];
    User *userTemp = [arrFriends objectAtIndex:indexPath.row];
    cell.profileImgView.layer.cornerRadius = 30;
    cell.profileImgView.image = [UIImage imageWithData:userTemp.avatar];
    cell.usernameLbl.text = [NSString stringWithFormat:@"%@ %@", userTemp.firstName, userTemp.lastName];
    if(userTemp.lastMessage.length > 0){
        cell.matchedOnLbl.font = [UIFont boldSystemFontOfSize:11];
        cell.matchedOnLbl.text = userTemp.lastMessage;
    }else{
        cell.matchedOnLbl.font = [UIFont systemFontOfSize:10];
        NSArray *arrMessages = [MessageController getConversationWithUser:userTemp withOffset:0 withCount:1];
        if(arrMessages.count > 0){
            cell.matchedOnLbl.text = [arrMessages.firstObject message];
        }else{
            NSDate *dateMatch = userTemp.matchOn;
            NSDateFormatter *dateFormatter = [ToolBox preconfiguredDateFormatter];
            dateFormatter.dateFormat = @"'Matched on' dd/MM/yyyy";
            cell.matchedOnLbl.text = [dateFormatter stringFromDate:dateMatch];
        }
    }
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ChatViewController *chatViewCont = [segue destinationViewController];
    chatViewCont.userFriend = [arrFriends objectAtIndex:[[self.tableView indexPathForSelectedRow] row]];
}
@end
