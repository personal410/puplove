//
//  Profile.m
//  bawow
//
//  Created by victor salazar on 25/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import "Profile.h"
@implementation Profile
@dynamic birthday;
@dynamic breed;
@dynamic descripcion;
@dynamic gender;
@dynamic name;
@dynamic pedigri;
@dynamic profileId;
@dynamic photos;
@dynamic user;
-(NSMutableDictionary *)dictionary{
    NSDateFormatter *dateFormat = [ToolBox preconfiguredDateFormatter];
    dateFormat.dateFormat = @"MM/dd/yyyy";
    NSMutableArray *arrImages = [NSMutableArray array];
    for(int i = 0; i < self.photos.allObjects.count; i++){
        [arrImages addObject:[NSNull null]];
    }
    for(Photo *photo in self.photos.allObjects){
        [arrImages replaceObjectAtIndex:photo.indPhoto.intValue withObject:[UIImage imageWithData:photo.photo]];
    }
    return [NSMutableDictionary dictionaryWithDictionary:@{@"name": self.name, @"breed": self.breed, @"gender": self.gender, @"birthday": [dateFormat stringFromDate:self.birthday], @"pedigri": self.pedigri, @"description": self.descripcion, @"indActual": @0, @"images": arrImages}];
}
@end