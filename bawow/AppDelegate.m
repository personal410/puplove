//
//  AppDelegate.m
//  bawow
//
//  Created by victor salazar on 30/12/14.
//  Copyright (c) 2014 victor salazar. All rights reserved.
//
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
@implementation AppDelegate
@synthesize managedObjectContext,managedObjectModel,persistentStoreCoordinator;
-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    [application setApplicationIconBadgeNumber:0];
    User *userTemp = [UserController getUser];
    FBSession *session = [FBSession activeSession];
    if(session.state == FBSessionStateCreatedTokenLoaded){
        [session openWithCompletionHandler:^(FBSession *s, FBSessionState status, NSError *e){}];
    }
    if(userTemp){
        if(userTemp.profile.profileId.length > 0){
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *viewCont = [storyboard instantiateViewControllerWithIdentifier:@"usuarioViewCont"];
            self.window.rootViewController = viewCont;
            [self.window makeKeyAndVisible];
        }else{
            [UserController deleteUser];
            [[FBSession activeSession] closeAndClearTokenInformation];
        }
    }
    return YES;
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSDictionary *dicParams = @{@"device_name": @"ios", @"device_token": token};
    NSURLRequest *req = [ToolBox createRequestWithUrl:[URLs urlAllowPushWithUserId:[[UserController getUser] userId]] withParams:[ToolBox dictionaryToPostParams:dicParams]];
    [ServerConnection addRequest:req WithCompletitionHandler:^(NSData *data, NSError *error){
        if(error){
            [ToolBox showConnectionErrorInViewCont:self];
        }
    }];
    
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    [ToolBox showAlertWith:@"Error" and:@"Could not register to remote notification" inViewCont:self.window.rootViewController];
}
-(void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
-(void)applicationDidEnterBackground:(UIApplication *)application{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
-(void)applicationWillEnterForeground:(UIApplication *)application{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
-(void)applicationDidBecomeActive:(UIApplication *)application{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
-(void)applicationWillTerminate:(UIApplication *)application{
}
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}
#pragma mark - Core Data
-(NSManagedObjectContext *)managedObjectContext{
    if(managedObjectContext != nil){
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if(coordinator != nil){
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}
-(NSManagedObjectModel *)managedObjectModel{
    if(managedObjectModel != nil){
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Bawow" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}
-(NSPersistentStoreCoordinator *)persistentStoreCoordinator{
    if(persistentStoreCoordinator != nil){
        return persistentStoreCoordinator;
    }
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Bawow.sqlite"];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]){
        abort();
    }
    return persistentStoreCoordinator;
}
-(NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
@end
