//
//  main.m
//  bawow
//
//  Created by victor salazar on 30/12/14.
//  Copyright (c) 2014 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
int main(int argc, char * argv[]){
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}