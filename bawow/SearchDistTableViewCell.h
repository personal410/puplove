//
//  SearchDistTableViewCell.h
//  bawow
//
//  Created by victor salazar on 29/08/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface SearchDistTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *valueSearchDistLbl;
@property(nonatomic,weak) IBOutlet UISlider *searchDistanceSlider;
@end