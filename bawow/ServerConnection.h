//
//  ServerConnection.h
//  bawow
//
//  Created by victor salazar on 14/03/15.
//  Copyright (c) 2015 victor salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
typedef void (^completitionHandler)(NSData *data, NSError *error);
@interface ServerConnection:NSObject{
    NSMutableArray *arrRequests, *arrCompletitionHandler;
    BOOL isConected;
}
+(id)sharedServerConnection;
+(void)addRequest:(NSURLRequest *)request WithCompletitionHandler:(completitionHandler)handler;
@end